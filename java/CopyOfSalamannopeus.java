package noobbot;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.tuple.Pair;

import msgs.CarPosition;
import msgs.Dimensions;
import msgs.Track.Segment;
import msgs.Track.Turn;

public class CopyOfSalamannopeus extends Main {

  private static final double CG = 0.05; //center of gravity should be low on slot car by default
  private static final double G = 9.80665; // Gravity constant approximation ;)
  private static final double TICK_LENGTH = 0.016;
  double VELOCITY_FACTOR ;
  double TURN_VELOCITY_FACTOR;
  double HARD_SLIDE;
  double VELO_MULTI;
  double SWITCH_FACTOR;
  double RISK_FACTOR;
  double TURN_LIMES;
  double vel_sum;
  int vel_count;
  List<Segment> switches = new ArrayList<Segment>();
  List<Pair> hop = new ArrayList<Pair>();
  int sindex = 0;
  private Pair selected;

  public CopyOfSalamannopeus(BufferedReader reader, PrintWriter writer, SendMsg join) throws IOException {
    super(reader, writer, join);
  }
	
  private boolean isTurn(int pieceIndex) {
    return track.isTurn(pieceIndex);
     
  }

  @Override
  protected void init() {
  RISK_FACTOR = 1.2755-0.06+(Math.random()*0.12); //not known forgot in night simulation
  HARD_SLIDE = 50.7;
  VELO_MULTI=1.74;
  TURN_LIMES=10.5;
  SWITCH_FACTOR=0.03;
  //6.506036627507814 50.67955907591164 1.738637118232973 0.1 enkat 0.785
  //Lars_Troll_Constants:   1.2855141230889287  10.527902985965772  1.74    0.03 enkat 7,48
  log.println( "Lars_Troll_Constants:\t"+RISK_FACTOR +"\t"+TURN_LIMES+"\t"+VELO_MULTI+"\t"+SWITCH_FACTOR);
  for( int i = 0; i < track.segments.size(); i++  ) {
    
    Segment s = track.segments.get(i);
    if( s.hasSwitch ) {
      switches.add(s);
    }
  }
  log.println( "Switcheja on " + switches.size());
  System.out.println( "Switcheja on " + switches.size());
  
  int len = switches.size();
  for (int i = 0;i<len;i++) {
      for (int j = i+1;j<len;j++) {
        System.out.println( i +" " +  j );
        hop.add( Pair.of(i, j));
        
       }
  }
  
  selected = Pair.of(1, 5);//*/hop.get((int)(Math.random()*hop.size()));
  log.println( "Sain kurvit: " + selected);
    
  }

  @Override
  protected void tick( String gameTick ) {
    CarPosition me = ourCarPosition;
    vel_sum+=me.velocity;
    vel_count++;
    double s = track.distanceToNextTurn(me);
    int nextPiece=(me.piecePosition.pieceIndex+1)%track.segments.size();
    boolean nextIsTurn=isTurn(nextPiece);
    Segment segment = track.segments.get(nextPiece);
    boolean isSwitch=track.segments.get(me.piecePosition.pieceIndex).hasSwitch;
    double throttle = 1.0;
    int lane_amount = track.lanes.length;
    double max_speed=nextIsTurn?calcMaxVelocity(segment,me,dimensions):1000.0;
    
    boolean inturn = isTurn(me.piecePosition.pieceIndex);
    
    //s=0.5at²== t=sqrt(2s/a) v=v0+at
    //two tick friction constant problem
    
    //double rough_time_to_turn=(s/me.velocity);

    //meta
    //ich Pwn

    //macro
    //if( isSwitch && Math.random()<SWITCH_FACTOR && (me.velocity-(RISK_FACTOR*max_speed))< 0.0) send( new SwitchLane(me.piecePosition.lane.startLaneIndex==0||me.piecePosition.lane.startLaneIndex<lane_amount-1?"Right":"Left"));
    /*if( isSwitch && switches.get((Integer)selected.getLeft()).id==me.piecePosition.pieceIndex && sindex==0 ) {
        send( new SwitchLane(me.piecePosition.lane.startLaneIndex==0||me.piecePosition.lane.startLaneIndex<lane_amount-1?"Right":"Left"));
        sindex++;
        System.out.println( "--------------------------------");
      }
    else if (isSwitch && switches.get((Integer)selected.getRight()).id==me.piecePosition.pieceIndex && sindex==1 ) {
        send( new SwitchLane(me.piecePosition.lane.startLaneIndex==1||me.piecePosition.lane.startLaneIndex<lane_amount-1?"Right":"Left"));
        sindex++;
        System.out.println( "--------------------------------");
    }

    
    //micro
    if( inturn && track.distanceToNextStrait(me)<100 ) {
      throttle=1.0;
    }
    else if ( rough_time_to_turn<TURN_LIMES) {
      throttle= ( (me.velocity-(RISK_FACTOR*max_speed))< 0.0)?1.0:0.0;
    }
    else {
      throttle=1.0;
    }
    
;
    send(new Throttle(throttle>1.0?1.0:throttle,gameTick));
  
    System.out.println(  (vel_sum/(double)vel_count) + " " + me.velocity +" " + throttle + " " + me.piecePosition.lane.startLaneIndex + " " + s +" " + me.angle + " ***********" + me.piecePosition.pieceIndex + " " + nextIsTurn + " " 
    + me.acceleration + " " + max_speed + " " + rough_time_to_turn);
  }

  private double calcMaxVelocity(Segment segment, CarPosition me, Dimensions dim) {
    Turn turn = (Turn)segment;
    //todo make radius calc be real( lane 0 or 1 )
    return  Math.sqrt((turn.radius)*G*dim.getAngularD()/CG)*TICK_LENGTH;   
  }
}
