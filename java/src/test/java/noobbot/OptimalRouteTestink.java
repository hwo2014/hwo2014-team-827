package noobbot;

import static java.lang.Math.abs;
import static java.lang.System.out;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

import java.io.ByteArrayOutputStream;
import java.io.PrintWriter;
import java.util.HashSet;
import java.util.Set;

import msgs.CarPosition;
import msgs.CarPosition.Lane;
import msgs.CarPosition.Position;
import msgs.TrackRoute;
import msgs.TrackRoute.SwitchOn;

import org.junit.Test;

import com.google.gson.Gson;

public class OptimalRouteTestink {
  Main code = new Main(null, null) {
    @Override
    protected void init() {
    }

    @Override
    protected void tick(String gameTick) {
    }
  };

  @Test
  public void keimolaTrack() {
    String track = "{\"msgType\":\"gameInit\",\"data\":{\"race\":{\"track\":{\"id\":\"keimola\",\"name\":\"Keimola\",\"pieces\":[{\"length\":100.0},{\"length\":100.0},{\"length\":100.0},{\"length\":100.0,\"switch\":true},{\"radius\":100,\"angle\":45.0},{\"radius\":100,\"angle\":45.0},{\"radius\":100,\"angle\":45.0},{\"radius\":100,\"angle\":45.0},{\"radius\":200,\"angle\":22.5,\"switch\":true},{\"length\":100.0},{\"length\":100.0},{\"radius\":200,\"angle\":-22.5},{\"length\":100.0},{\"length\":100.0,\"switch\":true},{\"radius\":100,\"angle\":-45.0},{\"radius\":100,\"angle\":-45.0},{\"radius\":100,\"angle\":-45.0},{\"radius\":100,\"angle\":-45.0},{\"length\":100.0,\"switch\":true},{\"radius\":100,\"angle\":45.0},{\"radius\":100,\"angle\":45.0},{\"radius\":100,\"angle\":45.0},{\"radius\":100,\"angle\":45.0},{\"radius\":200,\"angle\":22.5},{\"radius\":200,\"angle\":-22.5},{\"length\":100.0,\"switch\":true},{\"radius\":100,\"angle\":45.0},{\"radius\":100,\"angle\":45.0},{\"length\":62.0},{\"radius\":100,\"angle\":-45.0,\"switch\":true},{\"radius\":100,\"angle\":-45.0},{\"radius\":100,\"angle\":45.0},{\"radius\":100,\"angle\":45.0},{\"radius\":100,\"angle\":45.0},{\"radius\":100,\"angle\":45.0},{\"length\":100.0,\"switch\":true},{\"length\":100.0},{\"length\":100.0},{\"length\":100.0},{\"length\":90.0}],\"lanes\":[{\"distanceFromCenter\":-10,\"index\":0},{\"distanceFromCenter\":10,\"index\":1}],\"startingPoint\":{\"position\":{\"x\":-300.0,\"y\":-44.0},\"angle\":90.0}},\"cars\":[{\"id\":{\"name\":\"Tiihis\",\"color\":\"red\"},\"dimensions\":{\"length\":40.0,\"width\":20.0,\"guideFlagPosition\":10.0}}],\"raceSession\":{\"laps\":3,\"maxLapTimeMs\":60000,\"quickRace\":true}}},\"gameId\":\"bd4eb1d6-c857-471a-a05b-f0e6a6e16e0b\"}";
    TrackRoute r = calcRoutes(track);
    assertValid(r);
    assertThat(r.switches.get(0), is(new TrackRoute.SwitchOn(8,1,0)));
    assertThat(r.switches.get(1), is(new TrackRoute.SwitchOn(18,0,1)));
    assertThat(r.switches.size(), is(2));
    assertThat(r.sections.size(), is(17));
  }

  @Test
  public void germanyTrack() {
    String track = "{\"msgType\":\"gameInit\",\"data\":{\"race\":{\"track\":{\"id\":\"germany\",\"name\":\"Germany\",\"pieces\":[{\"length\":100.0},{\"length\":100.0,\"switch\":true},{\"radius\":50,\"angle\":45.0},{\"radius\":50,\"angle\":45.0},{\"radius\":50,\"angle\":45.0},{\"length\":50.0},{\"radius\":50,\"angle\":-45.0},{\"radius\":50,\"angle\":-45.0},{\"radius\":50,\"angle\":-45.0},{\"length\":50.0},{\"radius\":50,\"angle\":-45.0},{\"radius\":50,\"angle\":-45.0},{\"radius\":100,\"angle\":-22.5},{\"radius\":50,\"angle\":45.0},{\"length\":100.0,\"switch\":true},{\"length\":100.0},{\"radius\":50,\"angle\":-45.0},{\"length\":50.0},{\"radius\":50,\"angle\":45.0},{\"radius\":50,\"angle\":45.0},{\"radius\":100,\"angle\":22.5},{\"length\":100.0,\"switch\":true},{\"length\":100.0},{\"radius\":50,\"angle\":45.0},{\"radius\":50,\"angle\":45.0},{\"radius\":50,\"angle\":45.0},{\"radius\":50,\"angle\":45.0},{\"length\":100.0,\"switch\":true},{\"radius\":50,\"angle\":-45.0},{\"radius\":50,\"angle\":-45.0},{\"length\":50.0},{\"radius\":100,\"angle\":22.5},{\"length\":100.0},{\"length\":50.0},{\"radius\":100,\"angle\":-22.5},{\"radius\":100,\"angle\":-22.5},{\"length\":100.0,\"switch\":true},{\"radius\":50,\"angle\":45.0},{\"radius\":50,\"angle\":45.0},{\"radius\":100,\"angle\":22.5},{\"length\":100.0},{\"length\":100.0},{\"length\":100.0},{\"radius\":100,\"angle\":45.0},{\"length\":70.0},{\"length\":100.0,\"switch\":true},{\"radius\":50,\"angle\":-45.0},{\"radius\":50,\"angle\":-45.0},{\"radius\":100,\"angle\":45.0},{\"radius\":100,\"angle\":45.0},{\"length\":50.0},{\"radius\":100,\"angle\":45.0},{\"radius\":100,\"angle\":45.0},{\"radius\":100,\"angle\":45.0,\"switch\":true},{\"length\":100.0},{\"length\":100.0},{\"length\":59.0}],\"lanes\":[{\"distanceFromCenter\":-10,\"index\":0},{\"distanceFromCenter\":10,\"index\":1}],\"startingPoint\":{\"position\":{\"x\":-12.0,\"y\":192.0},\"angle\":-90.0}},\"cars\":[{\"id\":{\"name\":\"Tiihis\",\"color\":\"red\"},\"dimensions\":{\"length\":40.0,\"width\":20.0,\"guideFlagPosition\":10.0}}],\"raceSession\":{\"laps\":3,\"maxLapTimeMs\":60000,\"quickRace\":true}}},\"gameId\":\"5b16174f-9f66-422e-95db-5843aaecd75c\"}";
    TrackRoute r = calcRoutes(track);
    assertValid(r);
    assertThat(r.switches.get(0), is(new TrackRoute.SwitchOn(1,1,0)));
    assertThat(r.switches.get(1), is(new TrackRoute.SwitchOn(14,0,1)));
    assertThat(r.switches.get(2), is(new TrackRoute.SwitchOn(27,1,0)));
    assertThat(r.switches.get(3), is(new TrackRoute.SwitchOn(36,0,1)));
    assertThat(r.switches.size(), is(4));
    assertThat(r.sections.size(), is(34));
  }

  @Test
  public void usaTrack() {
    String track = "{\"msgType\":\"gameInit\",\"data\":{\"race\":{\"track\":{\"id\":\"usa\",\"name\":\"USA\",\"pieces\":[{\"length\":100.0},{\"length\":100.0,\"switch\":true},{\"radius\":200,\"angle\":22.5},{\"radius\":200,\"angle\":22.5},{\"radius\":200,\"angle\":22.5},{\"radius\":200,\"angle\":22.5},{\"length\":100.0},{\"length\":100.0,\"switch\":true},{\"radius\":200,\"angle\":22.5},{\"radius\":200,\"angle\":22.5},{\"radius\":200,\"angle\":22.5},{\"radius\":200,\"angle\":22.5},{\"length\":100.0},{\"length\":100.0},{\"length\":100.0},{\"length\":100.0},{\"length\":100.0},{\"length\":100.0,\"switch\":true},{\"radius\":200,\"angle\":22.5},{\"radius\":200,\"angle\":22.5},{\"radius\":200,\"angle\":22.5},{\"radius\":200,\"angle\":22.5},{\"length\":100.0},{\"length\":100.0,\"switch\":true},{\"radius\":200,\"angle\":22.5},{\"radius\":200,\"angle\":22.5},{\"radius\":200,\"angle\":22.5},{\"radius\":200,\"angle\":22.5},{\"length\":100.0},{\"length\":100.0},{\"length\":100.0},{\"length\":100.0}],\"lanes\":[{\"distanceFromCenter\":-20,\"index\":0},{\"distanceFromCenter\":0,\"index\":1},{\"distanceFromCenter\":20,\"index\":2}],\"startingPoint\":{\"position\":{\"x\":-340.0,\"y\":-96.0},\"angle\":90.0}},\"cars\":[{\"id\":{\"name\":\"Tiihis\",\"color\":\"red\"},\"dimensions\":{\"length\":40.0,\"width\":20.0,\"guideFlagPosition\":10.0}}],\"raceSession\":{\"laps\":3,\"maxLapTimeMs\":60000,\"quickRace\":true}}},\"gameId\":\"084ea1df-dc1a-4628-8014-938976d45518\"}";
    TrackRoute r = calcRoutes(track);
    assertValid(r);
    assertThat(r.switches.size(), is(0));
    assertThat(r.startLane, is(2));
    assertThat(r.sections.size(), is(9));

    CarPosition car = new CarPosition();
    car.piecePosition = new Position();
    car.piecePosition.lane = new Lane();
    car.piecePosition.lane.startLaneIndex = 0;
    car.piecePosition.lane.endLaneIndex = 0;
    TrackRoute lane0 = code.getOptimalRoute(car);
    assertThat(lane0.switches.size(), is(2));
    assertThat(lane0.switches.get(0), is(new TrackRoute.SwitchOn(1,0,1)));
    assertThat(lane0.switches.get(1), is(new TrackRoute.SwitchOn(7,1,2)));

    car.piecePosition.lane.startLaneIndex = 1;
    car.piecePosition.lane.endLaneIndex = 1;
    TrackRoute lane1 = code.getOptimalRoute(car);
    assertThat(lane1.switches.size(), is(1));
    assertThat(lane1.switches.get(0), is(new TrackRoute.SwitchOn(1,1,2)));

    car.piecePosition.pieceIndex = 2;
    car.piecePosition.inPieceDistance = 4.45720291248827;
    car.piecePosition.lane.startLaneIndex = 2;
    car.piecePosition.lane.endLaneIndex = 2;

    TrackRoute r2 = code.getOptimalRoute(car);
    assertThat(r2.switches.size(), is(0));
    assertThat(r2.startLane, is(2));
    assertThat(r2.sections.size(), is(9));

    car.piecePosition.pieceIndex = 1;
    car.piecePosition.inPieceDistance = 2.2389578246665707;
    car.piecePosition.lane.startLaneIndex = 0;
    car.piecePosition.lane.endLaneIndex = 1;

    TrackRoute r3 = code.getOptimalRoute(car);
    assertThat(r3.switches.size(), is(2));
    assertThat(r3.switches.get(0), is(new TrackRoute.SwitchOn(1,0,1)));
    assertThat(r3.switches.get(1), is(new TrackRoute.SwitchOn(7,1,2)));
  }

  private void assertValid(TrackRoute r) {
    assertThat(r.startLane, is(r.endLane));
    assertValidSwitches(r);
  }

  private void assertValidSwitches(TrackRoute r) {
    if (r.switches.isEmpty()) {
      assertThat(r.startLane, is(r.endLane));
    } else {
      assertThat(r.startLane, is(r.switches.get(0).fromTrack));
      assertThat(r.endLane, is(r.switches.get(r.switches.size() - 1).toTrack));
    }
    Set<Integer> seen = new HashSet<>();
    for (SwitchOn s : r.switches) {
      assertTrue("Same piece twice " + s.pieceId, seen.add(s.pieceId));
      assertThat(abs(s.fromTrack-s.toTrack), is(1));
    }
  }

  private TrackRoute calcRoutes(String track) {
    code.log = new PrintWriter(new ByteArrayOutputStream());
    MsgWrapper msg = new Gson().fromJson(track, MsgWrapper.class);
    code.handleInit(msg);
    out.println(code.routes.first());
    for (TrackRoute r : code.routes) {
      assertValidSwitches(r);
    }
    CarPosition car = new CarPosition();
    car.piecePosition = new Position();
    car.piecePosition.lane = new Lane();
    car.piecePosition.lane.startLaneIndex = code.track.bestRoute.startLane;
    car.piecePosition.lane.endLaneIndex = code.track.bestRoute.startLane;
    TrackRoute optimalRoute = code.getOptimalRoute(car);
    assertSame(optimalRoute, code.track.bestRoute);
    return code.track.bestRoute;
  }
}
