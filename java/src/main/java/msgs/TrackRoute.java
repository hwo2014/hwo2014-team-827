package msgs;

import static org.apache.commons.lang3.builder.EqualsBuilder.reflectionEquals;
import static org.apache.commons.lang3.builder.HashCodeBuilder.reflectionHashCode;

import java.util.ArrayList;

import msgs.CarPosition.Position;

public class TrackRoute implements Cloneable, Comparable<TrackRoute> {
  public static class SwitchOn {
    public int pieceId;
    public int fromTrack;
    public int toTrack;

    public SwitchOn(int pieceId, int fromTrack, int toTrack) {
      this.pieceId = pieceId;
      this.fromTrack = fromTrack;
      this.toTrack = toTrack;
    }

    @Override
    public boolean equals(Object obj) {
      return reflectionEquals(this, obj);
    }

    @Override
    public int hashCode() {
      return reflectionHashCode(this);
    }

    @Override
    public String toString() {
      return "Switch("+fromTrack+"=>" + toTrack + "@" + pieceId + ")";
    }
  }

  public int getSwitchCommand(int pieceIndex) {
    for (SwitchOn switchOn : switches) {
      if (switchOn.pieceId == pieceIndex) {
        return switchOn.toTrack - switchOn.fromTrack;
      }
    }
    return 0;
  }

  public double length;
  public double boost;

  public int piecePosition;

  public ArrayList<SwitchOn> switches = new ArrayList<>();
  public int startLane;
  public int endLane;

  public ArrayList<Section> sections;

  public boolean containsPosition(Position position, boolean onlyEndLaneMatters) {
    int lane = startLane;
    for (SwitchOn sw : switches) {
      if (position.pieceIndex > sw.pieceId) {
        lane = sw.toTrack;
      } else if (position.pieceIndex == sw.pieceId) {
        return (onlyEndLaneMatters || sw.fromTrack == position.lane.startLaneIndex) && sw.toTrack == position.lane.endLaneIndex;
      } else {
        break;
      }
    }
    return (onlyEndLaneMatters || lane == position.lane.startLaneIndex) && lane == position.lane.endLaneIndex;
  }

  public double getRemainingLenthFrom(Position position) {
    double d = 0;
    boolean apply = false;
    for (Section s : sections) {
      if (s.firstPiece <= position.pieceIndex && position.pieceIndex <= s.lastPiece) {
        apply = true;
        d = s.length - (s.startTrackPosition[position.pieceIndex - s.firstPiece] - s.trackPosition()) - position.inPieceDistance;
      } else if (apply) {
        d += s.length;
      }
    }
    return d;
  }

  @Override
  @SuppressWarnings("unchecked")
  public TrackRoute clone() {
    try {
      TrackRoute n = (TrackRoute) super.clone();
      n.switches = (ArrayList<SwitchOn>) switches.clone();
      return n;
    } catch (CloneNotSupportedException e) {
      throw new RuntimeException(e);
    }
  }

  @Override
  public int compareTo(TrackRoute o) {
    if (length + boost < o.length + boost)
      return -1;
    if (length + boost > o.length + boost)
      return 1;
    return switches.size() - o.switches.size();
  }

  @Override
  public boolean equals(Object obj) {
    TrackRoute o = (TrackRoute) obj;
    return length == o.length && boost == o.boost && switches.equals(o.switches);
  }

  @Override
  public int hashCode() {
    return (int) (length + boost) + switches.hashCode();
  }

  @Override
  public String toString() {
    return "Route[length=" + length + "/" + boost + "][startLane=" + startLane + "][switches=" + switches + "]";
  }
}
