package msgs;

import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.Map;

import msgs.Race.RaceSession;

import com.google.common.base.Joiner;
import com.google.gson.JsonArray;

public class Track {
  public String name;
  public String id;
  public JsonArray pieces;
  public Lane[] lanes;
  public Map<Integer, Segment> segments = new LinkedHashMap<>();
  public RaceSession raceSession;
  public TrackRoute bestRoute;
  public TrackRoute[] firstRoundRouteForLane;

  public static class Segment extends ToStringAble{
    public int id;
    public boolean hasSwitch;
    public double[] lengths;
    public transient double[][] switchLengths;
    public boolean isTurn;
    public double angle;
    public double radius;
    public double radiuses[];
    public transient double[][] switchRadiuses;

    public transient Segment next;
    public transient Segment prev;
  }

  public boolean isTurn(int piece) {
    return segments.get(piece).isTurn;
  }

  public boolean hasSwitch(int piece) {
    return segments.get(piece).hasSwitch;
  }

  public int nextTurn(CarPosition pos) {
    return nextTurn(pos.piecePosition.pieceIndex);
  }

  public int nextTurn(int currentPiece) {
    int nextTurn = currentPiece;
    while (true) {
      if (isTurn(nextTurn)) {
        return nextTurn;
      }
      nextTurn = nextSegment(nextTurn);
    }
  }

  public int nextSwitch(int currentPiece) {
    int nextSwitch = currentPiece;
    while (true) {
      if (hasSwitch(nextSwitch)) {
        return nextSwitch;
      }
      nextSwitch = nextSegment(nextSwitch);
    }
  }

  public int nextStrait(CarPosition pos) {
    return nextStrait(pos.piecePosition.pieceIndex);
  }

  public int nextStrait(int currentPiece) {
    int nextTurn = currentPiece;
    while (true) {
      if (!isTurn(nextTurn)) {
        return nextTurn;
      }
      nextTurn = nextSegment(nextTurn);
    }
  }

  public int nextSegment(int segmentNum) {
    segmentNum++;
    return segmentNum >= segments.size() ? 0 : segmentNum;
  }

  public int previousSegment(int segmentNum) {
    segmentNum--;
    return segmentNum < 0 ? segments.size() - 1 : segmentNum;
  }

  public double distanceTo(CarPosition pos, int toPiece) {
    return distanceTo(pos.piecePosition.pieceIndex, pos.piecePosition.inPieceDistance, toPiece, pos.piecePosition.lane.endLaneIndex, pos.piecePosition.lap);
  }

  public double distanceTo(int fromPiece, double fromPosition, int toPiece, int lane, int lap) {
    if (fromPiece == toPiece) {
      return 0;
    }
    double distance = segments.get(fromPiece).lengths[lane] - fromPosition;
    while (true) {
      int nextPiece = nextSegment(fromPiece);
      if (nextPiece < fromPiece) {
        lap++;
        if (raceSession.isLastLap(lap)) {
          return 10000000.0f;
        }
      }
      fromPiece = nextPiece;
      if (fromPiece == toPiece) {
        return distance;
      }
      distance += segments.get(fromPiece).lengths[lane];
    }
  }

  public double distanceToNextTurn(CarPosition pos) {
    return distanceTo(pos.piecePosition.pieceIndex, pos.piecePosition.inPieceDistance, nextTurn(pos), pos.piecePosition.lane.endLaneIndex, pos.piecePosition.lap);
  }

  public double distanceToNextStrait(CarPosition pos) {
    return distanceTo(pos.piecePosition.pieceIndex, pos.piecePosition.inPieceDistance, nextStrait(pos), pos.piecePosition.lane.endLaneIndex, pos.piecePosition.lap);
  }

  public double avgRadius(int pieceIndex, int startLaneIndex, int endLaneIndex) {
    Segment s = segments.get(pieceIndex);
    if (!s.isTurn) {
      return 0;
    }
    return s.switchRadiuses[startLaneIndex][endLaneIndex];
  }

  public double radius(int pieceIndex, int startLaneIndex, int endLaneIndex, double inPiecePosition) {
    Segment s = segments.get(pieceIndex);
    if (!s.isTurn) {
      // TODO: return something for strait that has a switch we are taking
      return 0;
    }
    if (startLaneIndex == endLaneIndex) {
      return s.radiuses[startLaneIndex];
    }
    double relPos = inPiecePosition / s.switchLengths[startLaneIndex][endLaneIndex];
    return s.radiuses[startLaneIndex] * (1 - relPos) + s.radiuses[endLaneIndex] * relPos;
  }

  public double radius(CarPosition pos) {
    return radius(pos.piecePosition.pieceIndex, pos.piecePosition.lane.startLaneIndex, pos.piecePosition.lane.endLaneIndex, pos.piecePosition.inPieceDistance);
  }

  public static class Lane extends ToStringAble {
    public int index;
    public double distanceFromCenter;
  }

  @Override
  public String toString() {
    return name + "\n" + Joiner.on("\n").join(segments.values()) + "\n\n" + Joiner.on("\n").join(lanes) + "\n\nbestRoute=" + bestRoute + "\nfirstLaneRoutes=" + Arrays.toString(firstRoundRouteForLane);
  }
}
