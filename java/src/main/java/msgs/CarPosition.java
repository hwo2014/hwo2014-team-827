package msgs;


public class CarPosition extends ToStringAble {
  public Position piecePosition;
  public double velocity;
  public double acceleration;
  public double angle;
  public double angleVelocity;
  public int routeSwitchedLap;
  public int ticksForSlipAngle;
  public Info id;

  public Section section;
  public double sectionPosition;
  public boolean isLeadingUs;
  public Turbo turbo = new Turbo();
  public Boolean alive;
  public int bumpedTime;

  public static class Info extends ToStringAble {
    public String name;
    public String color;
  }

  public static class Position extends ToStringAble {
    public int lap;
    public int pieceIndex;
    public double inPieceDistance;
    public Lane lane;

    public boolean isLeading(Position other) {
      return lap > other.lap || pieceIndex > other.pieceIndex || inPieceDistance > other.inPieceDistance;
    }

    public boolean isAheadOf(Position other) {
      return pieceIndex > other.pieceIndex || inPieceDistance > other.inPieceDistance;
    }
  }

  public static class Lane extends ToStringAble {
    public int startLaneIndex;
    public int endLaneIndex;
  }
}
