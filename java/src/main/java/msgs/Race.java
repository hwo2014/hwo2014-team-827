package msgs;

public class Race {
  public Track track;
  public Car[] cars;
  public RaceSession raceSession;

  public static class RaceSession {
    public int laps;
    public boolean quickRace;
    public int durationMs;
    public boolean isLastLap(int lap) {
      return lap >= laps;
    }
  }
}
