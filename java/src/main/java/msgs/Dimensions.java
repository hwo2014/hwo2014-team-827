package msgs;

//cars":[{"id":{"name":"Hinuri","color":"red"},"dimensions":{"length":40.0,"width":20.0,"guideFlagPosition":10.0}}]
public class Dimensions {
  public double length;
  double width;
  double guideFlagPosition;

  public double getStraitD() { //so with 40 and 10 becomes 10 =)
    return length*(guideFlagPosition/length);
  }

  public double getAngularD() { //angular based on slide angle or if normal then 0.5 =10
    return width*0.5;
  }

  public double getCurrentD(double slide_angle_in_degrees) {
    double sina =Math.sin(slide_angle_in_degrees*2.0*Math.PI/360.0);
    double cosa =Math.cos(slide_angle_in_degrees*2.0*Math.PI/360.0);

    double aa = getAngularD()*sina;
    double bb = getStraitD()*cosa;
    //System.out.printf("%f %f %f %f %f\n", Math.sqrt(Math.pow(aa,2.0)+Math.pow(bb,2.0)),aa,bb,getAngularD(), getStraitD() );
    // TODO Auto-generated method stub
    return bb;
  }

}
