package msgs;

import static org.apache.commons.lang3.builder.ToStringBuilder.reflectionToString;
import static org.apache.commons.lang3.builder.ToStringStyle.SHORT_PREFIX_STYLE;

public class ToStringAble {
  public String toString() {
    return reflectionToString(this, SHORT_PREFIX_STYLE);
  }
}
