package msgs;

public class Turbo implements Cloneable {
  public double turboFactor;
  public int turboDurationTicks;
  public double turboDurationMilliseconds;
  public boolean turboOn = false;
  public int turboTicksLeft;

  @Override
  public Turbo clone() {
    Turbo t = new Turbo();
    t.turboDurationMilliseconds = this.turboDurationMilliseconds;
    t.turboDurationTicks = this.turboDurationTicks;
    t.turboFactor = this.turboFactor;
    t.turboOn = this.turboOn;
    t.turboTicksLeft = this.turboTicksLeft;
    return t;
  }
}
