package msgs;

import static java.lang.Math.signum;
import static java.util.Arrays.copyOf;
import static msgs.Section.SectionType.strait;
import static msgs.Section.SectionType.turn;
import msgs.Track.Segment;


public class Section implements Cloneable {
  public enum SectionType {
    strait, turn
  }

  public Section(Segment piece, double trackPos) {
    type = piece.isTurn ? turn  : strait;
    firstPiece = lastPiece = piece.id;
    angle = piece.angle;
    startTrackPosition = new double[]{trackPos};
  }

  public SectionType type;
  public double length;
  public double angle;
  public double avgRadius;
  public double startRadius;
  public double endRadius;
  public boolean hasSwitch;
  public int nthLongest;

  public transient Section next;

  public int firstPiece;
  public int lastPiece;
  public double[] startTrackPosition;

  public double trackPosition() {
    return startTrackPosition[0];
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder(256);
    sb.append("Sect[").append(firstPiece);
    if (lastPiece != firstPiece) {
      sb.append("-").append(lastPiece);
    }
    sb.append("@").append(trackPosition());
    sb.append("[len=").append(length);
    if (nthLongest >= 1 && nthLongest < 10) {
      String type = nthLongest == 1 ? "st" : (nthLongest == 2 ? "nd" : (nthLongest == 3 ? "rd" : "th"));
      sb.append("(").append(nthLongest).append(type).append(" longest)");
    }
    sb.append("]");
    if (hasSwitch) {
      sb.append("[hasSwitch]");
    }
    if (angle != 0) {
      sb.append("[angle=" + angle);
      sb.append("][radius=");
      if (hasSwitch) {
        sb.append(startRadius).append("-").append(endRadius);
      } else {
        sb.append(avgRadius);
      }
      sb.append("]");
    }
    return sb.toString();
  }

  public boolean add(Section other) {
    if (!canMerge(other)) {
      return false;
    }
    length += other.length;
    lastPiece = other.lastPiece;
    angle += other.angle;
    startTrackPosition = copyOf(startTrackPosition, startTrackPosition.length + 1);
    startTrackPosition[startTrackPosition.length - 1] = other.trackPosition();
    return true;
  }

  public boolean addLength(Section other) {
    if (canMerge(other)) {
      length += other.length;
      return true;
    }
    return false;
  }

  private boolean canMerge(Section other) {
    if (type != other.type || hasSwitch || other.hasSwitch || avgRadius != other.avgRadius || signum(angle) != signum(other.angle))
      return false;
    return true;
  }

  @Override
  protected Section clone() {
    try {
      return (Section) super.clone();
    } catch (CloneNotSupportedException e) {
      throw new RuntimeException(e);
    }
  }
}
