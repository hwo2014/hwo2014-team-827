package msgs;

public class TurboHistory {
  public double turboPreThrottle = 0.0;
  public int turboSegment = 0;
  public boolean turboWasSafe = true;
  public int turboTick;
  public int turboDuration;

}
