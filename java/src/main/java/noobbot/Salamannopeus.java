package noobbot;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;

import msgs.CarPosition;
import msgs.Section.SectionType;
import msgs.Track.Segment;


public class Salamannopeus extends AbstractBot {

  private static double MIN_THROTTLE = 0.00;
  private static double MAX_THROTTLE = 1.00;
  private static double RISK_FACTOR = 1.0;
  private static double ANGLE_SAFE_LIMES = 50.00;
  private static double ANGLE_MOD_LIMES = 40.00;
  private static double ANGLE_MAX_LIMES = 59.50;
  private static double vEPSILON = 0.05; //velocity EPSILON about 1 max accelration from 0( for average physics )
  private static double sEPSILON = 8.0; //distance EPSILON about 1/4 tick( averaged )
  private static double aEPSILON = 0.5; //angle EPSILON about half degrees
  private static int UPDATE_UNTIL = 1; //lap to start racing

  //metrics
  double prevThrottle;
  double prevSlipAngle;
  boolean checkUG=true;

  double velosum;
  long velocount;
  double max_angle;
  int max_angle_tick;
  double abs_angle_sum;
  long abs_angle_count;

  public Salamannopeus(BufferedReader reader, PrintWriter writer) throws IOException {
    super(reader, writer);
  }

  @Override
  protected void init() {
  //log.println( "Lars_Troll_Constants:\t"+PEPSILON +"\t"+MAX_DECELERATION+"\t"+MAGIC +"\t" +EPSILON+"\t"+RISK_FACTOR+"\t"+TURN_DEC+"\t"+TURN_ACC+"\n");
  }

  @Override
  protected void tick( String gameTick ) {

    int tick = 0;
    try {
      tick = Integer.parseInt(gameTick);
    } catch (Exception e) {
    }

    //check acc magic
    checkMagicNumbers(tick);
    checkAngularMagicNumbers();


    //timing and averages
    long start = System.currentTimeMillis();
    velosum+=ourCarPosition.velocity;
    velocount++;
    if( Math.abs(ourCarPosition.angle)>max_angle ) {
      max_angle=Math.abs(ourCarPosition.angle);
      max_angle_tick=tick;
    }

    abs_angle_sum+=Math.abs(ourCarPosition.angle);
    abs_angle_count++;

    //init
    double throttle = MAX_THROTTLE;

    double max_speed=RISK_FACTOR*maxSpeedAtTurn(ourCarPosition);
    double max_speed_next_turn=RISK_FACTOR*maxSpeedAtNextTurn(track.nextTurn(ourCarPosition));
    double dV = (ourCarPosition.velocity+vEPSILON)-max_speed_next_turn;
    double a = getAcceleration(ourCarPosition.velocity,0.0,ourCarPosition.turbo.turboFactor);
    double breaking_time = dV<0?0.0:dV/a;
    double breaking_distance=(ourCarPosition.velocity*breaking_time+0.5*Math.pow(breaking_time,2)*a);
    breaking_distance=breaking_distance>0.0?breaking_distance+sEPSILON:breaking_distance;

    //META -> doctrine and precalc
    //be fastest fear only turbo on overtakes

    //MACRO -> route and turbo and overtake decision
    //==============================================================
    boolean commandSent = false;
    if (sendRouteSwitchIfNeeded()) {
      commandSent = true;
    } else if (sendTurboIfNeeded(gameTick)) {
      commandSent = true;
    }
    overTakeOrBlockIfNeeded(gameTick);

    int lap = ourCarPosition.piecePosition.lap;


    //we update in first laps
    if(ourCarPosition.velocity>0.0 && lap<UPDATE_UNTIL &&track.isTurn(ourCarPosition.piecePosition.pieceIndex) && !ourCarPosition.turbo.turboOn) {
      C_UG=updateMagic(C_UG, max_angle,max_angle_tick,abs_angle_sum/abs_angle_count);
      }

    if( ourCarPosition.velocity>0.0 &&lap>=UPDATE_UNTIL ) {
      RISK_FACTOR=updateRisk(RISK_FACTOR, max_angle,max_angle_tick,abs_angle_sum/abs_angle_count);
    }
    //==============================================================

    String type = "";

    //MICRO -> Main car handling in micro scale
    CarPosition realNextCarPosition = getNextCarPosition(ourCarPosition, prevThrottle);
    if( gameTick==null || gameTick!=null && gameTick.equalsIgnoreCase("1") ) {
      throttle=MAX_THROTTLE;
      }
    else if(track.isTurn(ourCarPosition.piecePosition.pieceIndex) &&  track.distanceToNextStrait(ourCarPosition)<track.radius(ourCarPosition)*0.5 ) {
      type="CASE OUT OF BEND:";
      throttle=safeThrottle(1.0);
      }
    else if( !track.isTurn(ourCarPosition.piecePosition.pieceIndex) ) {
      type ="CASE JOLLY STRAIGHT:";
      boolean chenSuggestsToBreak = needBreak(realNextCarPosition);
      boolean larsSuggestToBreak = track.distanceToNextTurn(ourCarPosition)+breaking_distance<sEPSILON;
      type+=chenSuggestsToBreak + " " + larsSuggestToBreak;
      throttle= ( chenSuggestsToBreak && larsSuggestToBreak )?0.0:1.0;
      }
    else {
      //Case radius changes to narrower
      int nextAfter=(ourCarPosition.section.lastPiece+1)%track.segments.size();
      Segment s = track.segments.get(nextAfter);
      boolean next_section_narrower_turn = s.isTurn && s.radiuses[ourCarPosition.piecePosition.lane.endLaneIndex]<track.radius(ourCarPosition);
      double distanceToNarrower=(next_section_narrower_turn)?track.distanceTo(ourCarPosition, nextAfter):10000000.0;
      double speed_next = maxSpeedAtNextTurn(nextAfter);
      dV = (ourCarPosition.velocity+vEPSILON)-speed_next;
      a = getAcceleration(ourCarPosition.velocity,0.0,ourCarPosition.turbo.turboFactor);
      breaking_time = dV<0?0.0:dV/a;
      breaking_distance=(ourCarPosition.velocity*breaking_time+0.5*Math.pow(breaking_time,2)*a);
      breaking_distance=breaking_distance>0.0?breaking_distance+sEPSILON:breaking_distance;

      type="CASE DEEP IN TURN:"+breaking_distance+ " " + distanceToNarrower ;
      throttle=(( distanceToNarrower+breaking_distance ) < sEPSILON)?0.0:
        (ourCarPosition.velocity+vEPSILON)<max_speed?safeThrottle(1.0):
        (ourCarPosition.velocity>(max_speed+vEPSILON))?0.0:ourCarPosition.velocity/maxSpeed;
    }

    if (!commandSent)
      send(new Throttle((throttle>MAX_THROTTLE?MAX_THROTTLE:throttle<MIN_THROTTLE?MIN_THROTTLE:throttle),gameTick));

    //logging
    long delta = System.currentTimeMillis()-start;
    velosum+=ourCarPosition.velocity;
    velocount++;
    String msg = String.format("[%4d|%2d|%1d] T[%2.3f] V[%2.2f(%2.2f|%2.2f|%2.2f|%2.2f)|%2.2f][%3.2f] A[%2.2f|%3.2f]R[%2.1f] L[%1d|%1d] S[%2.2f|%2.2f|%2.2f] a[%2.2f|%3d][%2.2f] XX[%2.4f|%2.4f|%2.4f|%2.4f] %4dms %s\n",
        tick, ourCarPosition.piecePosition.pieceIndex, ourCarPosition.piecePosition.lap,
        throttle,
        ourCarPosition.velocity,
        maxSpeedAtTurn(ourCarPosition),maxSpeedAtTurn(ourCarPosition)*RISK_FACTOR,
        maxSpeedAtNextTurn(track.nextTurn(ourCarPosition)),maxSpeedAtNextTurn(track.nextTurn(ourCarPosition))*RISK_FACTOR,
        velosum/velocount,
        ourCarPosition.angleVelocity,
        ourCarPosition.angle,track.segments.get(ourCarPosition.piecePosition.pieceIndex).angle,track.segments.get(ourCarPosition.piecePosition.pieceIndex).radius,
        ourCarPosition.piecePosition.lane.startLaneIndex,ourCarPosition.piecePosition.lane.endLaneIndex,
        track.distanceToNextTurn(ourCarPosition),
        track.distanceToNextStrait(ourCarPosition),
        breaking_distance,
        max_angle,max_angle_tick,abs_angle_sum/abs_angle_count,
        C_UG, aEPSILON, RISK_FACTOR, ANGLE_MOD_LIMES,
        delta,
        type);
    System.out.print(msg);
    log.print(msg);

    prevThrottle = throttle;
    prevSlipAngle = ourCarPosition.angle;
  }

  private double updateRisk(double risk,  double maxBeta, int maxBetaPos, double averBeta) {
   double newRisk = risk;
   if( maxBeta<ANGLE_MOD_LIMES && Math.abs(averBeta)<aEPSILON ) {
     newRisk=risk*1.0002;
   }
   else if( ANGLE_MOD_LIMES<=ANGLE_MAX_LIMES ){
     ANGLE_MOD_LIMES*=1.05;
     newRisk=risk*0.995;
   }
   else if( Math.abs(averBeta)>aEPSILON  ) {
     aEPSILON*=1.02;
   }
   else {
     newRisk=1.05;
   }
   return newRisk;
  }

  private double updateMagic(double uG, double maxBeta, int maxBetaPos, double averBeta) {
    double newMagic = uG;
    if( Math.abs(averBeta)>aEPSILON ) {
        newMagic =uG*0.9998;
        aEPSILON*=1.02;
    }
    else {
      newMagic = uG*1.0002f;
    }
    return newMagic;

  }

  //clear coding of winners ;)
  private double safeThrottle(double proposedThrottle) {
    return track.isTurn(ourCarPosition.piecePosition.pieceIndex)?
        ( ourCarPosition.velocity<RISK_FACTOR*(maxSpeedAtTurn(ourCarPosition)+ourCarPosition.acceleration))?
          proposedThrottle:
          checkPendulumSafe(ourCarPosition)?
              proposedThrottle:
              ourCarPosition.velocity>RISK_FACTOR*maxSpeedAtTurn(ourCarPosition)?
                  0.0:
                  maxSpeedAtTurn(ourCarPosition)/maxSpeed:
        proposedThrottle;
  }


  private boolean sendTurboIfNeeded(String gametick) {
    if (ourCarPosition.turbo.turboFactor > 0f &&
        checkPendulumSafe(ourCarPosition)
        && getNextSectionOfType(SectionType.strait).nthLongest < 2
        && getNextSectionOfType(SectionType.strait).nthLongest > 0 && !ourCarPosition.turbo.turboOn
        && ( track.radius(ourCarPosition)== 0.0 || (track.distanceToNextStrait(ourCarPosition)<(track.radius(ourCarPosition)*0.5)))) {
      send(new ActivateTurbo(gametick));
      ourCarPosition.turbo.turboOn = true;
      return true;
    }
    return false;
  }

  @Override
  protected void overTakeOrBlockIfNeeded( String gametick ) {
    for (CarPosition enemy : carPositions) {
      if (enemy.id.equals(ourCarPosition.id)) {
        continue;
      } else {
        // jos sama lane tai tulossa samalla
        if (enemy.piecePosition.lane.endLaneIndex == ourCarPosition.piecePosition.lane.endLaneIndex) {
          if (enemy.piecePosition.pieceIndex == ourCarPosition.piecePosition.pieceIndex)
            if (enemy.piecePosition.inPieceDistance + 0.2 > ourCarPosition.piecePosition.inPieceDistance) {

              //if corner near and we fast enough, we try to turbo bump to oblivion
              if( 50.0<track.distanceToNextTurn(ourCarPosition) && track.distanceToNextTurn(ourCarPosition)<200.0 && enemy.velocity<(ourCarPosition.velocity*1.2) ) {
                send(new ActivateTurbo(gametick));
              }
              else if (Math.random() < 0.8) { // not always cos we so räikkönen
                send(new SwitchLane(ourCarPosition.piecePosition.lane.startLaneIndex == 0
                    || ourCarPosition.piecePosition.lane.startLaneIndex < track.lanes.length - 1 ? "Right" : "Left"));
              }
            }
        }
      }
    }
  }

  @Override
  protected void handleCrash() {
    if( ourCarPosition.piecePosition.lap<UPDATE_UNTIL) {
      C_UG*=0.98;
      }
    else {
      RISK_FACTOR*=0.91;
      }
    System.out.println( "Le crash");
  }


  protected double maxSpeedAtNextTurn( int index ) {
    return Math.sqrt(track.radius(index, ourCarPosition.piecePosition.lane.endLaneIndex, ourCarPosition.piecePosition.lane.endLaneIndex, 0) * C_UG );
  }

  @Override
  protected double maxSpeedAtTurn(CarPosition carPosition) {
    return Math.sqrt(track.radius(carPosition) * C_UG);
  }

}
