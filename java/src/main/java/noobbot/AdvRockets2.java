package noobbot;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;

import msgs.CarPosition;
import msgs.Section.SectionType;
import msgs.Track.Segment;
import msgs.TrackRoute.SwitchOn;

public class AdvRockets2 extends AbstractBot {
  private static final double SAFE_DIS_FACTOR = 2;
  private double SPEED_RISK_FACTOR = 1.45;
  private double SPEED_RISK_FACTORS[];
  private double ANGLE_RISK_FACTOR = 25;
  private double ANGLE_RISK_FACTORS[];
  private double MIN_ANGLE_RISK_FACTORS[];
  private double MAX_ANGLE_RISK_FACTOR = 45;
  private double maxLapAngle = 0;
  private double maxLapAngles[];
  private double desiredAngle = 50;
  private boolean isFirstLapFinished;

  public AdvRockets2(BufferedReader reader, PrintWriter writer) throws IOException {
    super(reader, writer);
  }

  @Override
  protected void init() {
    if (SPEED_RISK_FACTORS == null) {
      SPEED_RISK_FACTORS = new double[track.segments.size()];
      for (int i = 0; i < track.segments.size(); i++) {
        SPEED_RISK_FACTORS[i] = SPEED_RISK_FACTOR;
      }
    }
    if (ANGLE_RISK_FACTORS == null) {
      ANGLE_RISK_FACTORS = new double[track.segments.size()];
      for (int i = 0; i < track.segments.size(); i++) {
        ANGLE_RISK_FACTORS[i] = ANGLE_RISK_FACTOR;
      }
    }
    if (MIN_ANGLE_RISK_FACTORS == null) {
      MIN_ANGLE_RISK_FACTORS = new double[track.segments.size()];
      for (int i = 0; i < track.segments.size(); i++) {
        MIN_ANGLE_RISK_FACTORS[i] = 0;
      }
    }
    if (maxLapAngles == null) {
      maxLapAngles = new double[track.segments.size()];
      for (int i = 0; i < track.segments.size(); i++) {
        maxLapAngles[i] = 0;
      }
    }
    maxLapAngle = 0;
    isFirstLapFinished = true;
  }

  @Override
  protected void handleCrash() {
    logCarPosition(0, "---", ourCarPosition);
    System.out.println("Crash:" + track.name + ":" + C_UG + ":" + maxSpeed);
    int pi = ourCarPosition.piecePosition.pieceIndex;
    for (int i = 0; i < 5; i++) {
      resetFactorForCrash(pi);
      pi = track.previousSegment(pi);
    }
  }

  private void resetFactorForCrash(int pi) {
    SPEED_RISK_FACTORS[pi] = SPEED_RISK_FACTORS[pi] / 1.03;
    MIN_ANGLE_RISK_FACTORS[pi] = ANGLE_RISK_FACTORS[pi] + 1;
    ANGLE_RISK_FACTORS[pi] = ANGLE_RISK_FACTORS[pi] + 5;
    if (ANGLE_RISK_FACTORS[pi] >= MAX_ANGLE_RISK_FACTOR) {
      ANGLE_RISK_FACTORS[pi] = MAX_ANGLE_RISK_FACTOR;
    }
    System.out.println("piece" + pi + ":" + ANGLE_RISK_FACTORS[pi] + ":" + SPEED_RISK_FACTORS[pi]);
  }

  @Override
  protected void tournamentEnd() {
    System.out.println("End:" + track.name + ":" + C_UG + ":" + maxSpeed);
    super.tournamentEnd();
  }

  @Override
  protected void handleLapFinished() {
    System.out.println("LAP:" + track.name + ":" + maxLapAngle + ":" + C_UG + ":" + maxSpeed);
    for (int i = 0; i < track.segments.size(); i++) {
      double maxAngle = maxLapAngles[i];
      int ni = track.nextSegment(i);
      for (int j = 0; j < 5; j++) {
        maxAngle = Math.max(maxAngle, maxLapAngles[ni]);
        ni = track.nextSegment(ni);
      }
      if (isFirstLapFinished) {
        maxAngle = maxLapAngle;
      }
      double diff = desiredAngle - maxAngle;
      if (maxAngle < desiredAngle) {
        ANGLE_RISK_FACTORS[i] = ANGLE_RISK_FACTORS[i] - diff / 4;
        SPEED_RISK_FACTORS[i] = SPEED_RISK_FACTORS[i] + diff * 0.0004;
      } else {
        ANGLE_RISK_FACTORS[i] = ANGLE_RISK_FACTORS[i] - diff * 1.5;
        SPEED_RISK_FACTORS[i] = SPEED_RISK_FACTORS[i] + diff * 0.005;
      }
      ANGLE_RISK_FACTORS[i] = Math.max(MIN_ANGLE_RISK_FACTORS[i], ANGLE_RISK_FACTORS[i]);
    }
    for (int i = 0; i < track.segments.size(); i++) {
      maxLapAngles[i] = 0;
    }
    maxLapAngle = 0;
    isFirstLapFinished = false;
  }

  @Override
  protected void tick(String gameTick) {
    maxLapAngle = Math.max(maxLapAngle, Math.abs(ourCarPosition.angle));
    int pieceIndex = ourCarPosition.piecePosition.pieceIndex;
    maxLapAngles[pieceIndex] = Math.max(maxLapAngles[pieceIndex], Math.abs(ourCarPosition.angle));
    int tick = 0;
    try {
      tick = Integer.parseInt(gameTick);
    } catch (Exception e) {
    }

    checkAngularMagicNumbers();
    double turboFactor = 1;
    if (ourCarPosition.turbo.turboOn) {
      turboFactor = ourCarPosition.turbo.turboFactor;
    }
    double throttle = 1;
    int turnsToCheck = 4;
    for (int i = 0; i < turnsToCheck; i++) {
      int turnIndex = track.nextTurn(track.nextSegment(pieceIndex));
      for (int j = 0; j < i; j++) {
        turnIndex = track.nextTurn(track.nextSegment(turnIndex));
      }
      throttle = Math.min(throttle, maxThrottleForTurn(throttle, turnIndex, turboFactor));
    }
    int nextTurn = track.nextTurn(track.nextSegment(pieceIndex));
    if (ourCarPosition.angle * ourCarPosition.angleVelocity > 0
        && Math.abs(ourCarPosition.angle) + Math.abs(ourCarPosition.angleVelocity) * ANGLE_RISK_FACTORS[pieceIndex]
            * Math.sqrt(C_UG) > 57) {
      throttle = 0;
    } else if (track.isTurn(pieceIndex)) {
      if (ourCarPosition.angle * ourCarPosition.angleVelocity > 0
          && Math.abs(ourCarPosition.angle) + Math.abs(ourCarPosition.angleVelocity) * ANGLE_RISK_FACTORS[pieceIndex]
              * 3 * Math.sqrt(C_UG) > 57) {
        double maxSpeedAtTurnWithoutSlip = maxSpeedAtTurnWithoutSlip(ourCarPosition);
        throttle = Math.min(throttle,
            getThrottleForSpeedChange(maxSpeedAtTurnWithoutSlip, ourCarPosition.velocity, turboFactor));
      } else {
        double maxSpeedAtTurn = maxSpeedAtTurn(ourCarPosition);
        Segment currSegment = track.segments.get(pieceIndex);
        Segment nextSegment = track.segments.get(nextTurn);
        if ((track.distanceToNextStrait(ourCarPosition) > track.radius(ourCarPosition) / SAFE_DIS_FACTOR && (track
            .distanceTo(ourCarPosition, nextTurn) > track.radius(ourCarPosition) / SAFE_DIS_FACTOR || nextSegment.angle
            * currSegment.angle > 0))) {
          throttle = Math
              .min(throttle, getThrottleForSpeedChange(maxSpeedAtTurn, ourCarPosition.velocity, turboFactor));
        }
      }
    }

    boolean commandSent = false;
    if (checkMagicNumbers(tick)) {
      commandSent = true;
    } else if (sendRouteSwitchIfNeeded()) {
      commandSent = true;
    } else if (sendTurboIfNeeded(prevThrottle)) {
      commandSent = true;
    }

    if (!commandSent) {
      if (throttle > 1)
        throttle = 1;
      if (throttle < 0)
        throttle = 0;
      if (tick < 4)
        throttle = 1;

      send(new Throttle(throttle));
      // logCarPosition(throttle, gameTick, ourCarPosition);
      prevThrottle = throttle;
    }
  }

  private double maxThrottleForTurn(double throttle, int nextTurn, double turboFactor) {
    Segment segment = track.segments.get(nextTurn);
    double disToNextTurn = track.distanceTo(ourCarPosition, nextTurn);
    double maxSpeedAtNextTurn = maxSpeedAtNextTurn(nextTurn);
    double disToSlow = getBreakDistance(ourCarPosition.velocity, maxSpeedAtNextTurn);
    if (disToNextTurn < disToSlow) {
      throttle = getThrottleForSpeedChange(maxSpeedAtNextTurn, ourCarPosition.velocity, turboFactor);
      if (ourCarPosition.angle * segment.angle < 0) {
        throttle = throttle + Math.abs(ourCarPosition.angle) / 30;
      }
    }
    return throttle;
  }

  private double getBreakDistance(double velocity, double maxSpeedAtNextTurn) {
    double distance = 0;
    while (velocity > maxSpeedAtNextTurn) {
      distance = distance + velocity;
      velocity = getNextSpeed(velocity, 0, 1);
    }
    return distance + maxSpeedAtNextTurn;
  }

  private void logCarPosition(double throttle, String gameTick, CarPosition carPosition) {
    System.out.print(track.name);
    System.out.print(",");
    System.out.print(gameTick);
    System.out.print(",");
    System.out.format("Throttle=%.2f", throttle);
    System.out.print(",");
    System.out.print(carPosition.piecePosition.lap);
    System.out.print(",");
    System.out.print(carPosition.piecePosition.pieceIndex);
    System.out.print(",");
    System.out.print(carPosition.piecePosition.lane.startLaneIndex);
    System.out.print("->");
    System.out.print(carPosition.piecePosition.lane.endLaneIndex);
    System.out.print(",");
    System.out.format("%.3f", carPosition.piecePosition.inPieceDistance);
    System.out.print(",");
    System.out.format("%.3f", carPosition.velocity);
    System.out.print(",");
    System.out.format("%.3f", carPosition.acceleration);
    System.out.print(",");
    System.out.format("%.3f", carPosition.angle);
    System.out.print(",");
    System.out.format("%.3f", carPosition.angleVelocity);
    System.out.print(",");
    System.out.format("%.3f", track.radius(carPosition));
    System.out.print(",");
    System.out.format("%.3f", track.segments.get(carPosition.piecePosition.pieceIndex).angle);
    System.out.println();
  }

  protected double maxSpeedAtNextTurn(int nextTurn) {
    int index = ourCarPosition.piecePosition.pieceIndex;
    int lane = ourCarPosition.piecePosition.lane.endLaneIndex;
    int laneend = ourCarPosition.piecePosition.lane.endLaneIndex;
    while (index != nextTurn) {
      for (SwitchOn switchOn : route.switches) {
        if (switchOn.pieceId == index) {
          lane = switchOn.toTrack;
          laneend = switchOn.toTrack;
        }
      }
      index = track.nextSegment(index);
    }
    for (SwitchOn switchOn : route.switches) {
      if (switchOn.pieceId == nextTurn) {
        laneend = switchOn.toTrack;
      }
    }
    double radius = track.radius(nextTurn, lane, laneend, 0);
    return maxSpeedAtTurn(nextTurn, radius, 1);
  }

  @Override
  protected double maxSpeedAtTurn(CarPosition carPosition) {
    return maxSpeedAtTurn(carPosition.piecePosition.pieceIndex, track.radius(carPosition), 1);
  }

  protected double maxSpeedAtTurnWithoutSlip(CarPosition carPosition) {
    return maxSpeedAtTurn(carPosition.piecePosition.pieceIndex, track.radius(carPosition), 1.5);
  }

  protected double maxSpeedAtTurn(int pieceIndex, double radius, double factor) {
    Segment prev = track.segments.get(track.previousSegment(pieceIndex));
    Segment next = track.segments.get(track.nextSegment(pieceIndex));
    Segment curr = track.segments.get(pieceIndex);
    if ((!track.isTurn(track.nextSegment(pieceIndex)) || next.angle * curr.angle < 0)
        && (!track.isTurn(track.previousSegment(pieceIndex)) || prev.angle * curr.angle < 0)
        && track.segments.get(pieceIndex).lengths[ourCarPosition.piecePosition.lane.endLaneIndex] < radius
            / SAFE_DIS_FACTOR) {
      return maxSpeed;
    }
    double mspeed = Math.sqrt(radius * C_UG * SPEED_RISK_FACTORS[pieceIndex] / factor);
    return mspeed;
  }

  @Override
  protected boolean sendTurboIfNeeded(double throttle) {
    CarPosition nextCarPosition = getNextCarPosition(ourCarPosition, throttle);
    if (ourCarPosition.turbo.turboFactor > 0.1 && !track.isTurn(ourCarPosition.piecePosition.pieceIndex)
        && track.distanceToNextTurn(ourCarPosition) > maxSpeed * ourCarPosition.turbo.turboDurationTicks * 0.75
        && isSafeToActivateTurbo(nextCarPosition) && ourCarPosition.section.nthLongest < 2
        && ourCarPosition.section.nthLongest > 0 && !ourCarPosition.turbo.turboOn) {
      send(new ActivateTurbo());
      ourCarPosition.turbo.turboOn = true;
      return true;
    }

    if (ourCarPosition.turbo.turboFactor > 0.1 && track.isTurn(ourCarPosition.piecePosition.pieceIndex)
        && (track.distanceToNextStrait(ourCarPosition) < (track.radius(ourCarPosition) * 0.5))
        && isSafeToActivateTurbo(nextCarPosition)) {
      int nextTurn = track.nextTurn(track.nextStrait(ourCarPosition));
      if (track.distanceTo(ourCarPosition, nextTurn) > maxSpeed * ourCarPosition.turbo.turboDurationTicks * 0.75
          && getNextSectionOfType(SectionType.strait).nthLongest < 2
          && getNextSectionOfType(SectionType.strait).nthLongest > 0 && !ourCarPosition.turbo.turboOn) {
        send(new ActivateTurbo());
        ourCarPosition.turbo.turboOn = true;
        return true;
      }
    }
    return false;
  }
}
