package noobbot;

import java.io.BufferedReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

import msgs.CarPosition;
import msgs.Section.SectionType;
import msgs.TrackRoute.SwitchOn;
import msgs.Turbo;

public class Hinuri extends Main {
  private static final double DISTANCE_TO_STRAIT = 45.0;
  double velosum;
  long velocount;
  public static final String LASTRUN_LOG = "Rockets_lastrun.log";
  private static final double MAGIC = 0.053f;//0.052f;
  // finland 7:05
  // private static final double MAGIC = 0.047f;
  // germany 9:76
  // private static final double MAGIC = 0.049f;
  // usa 4:60
  // private static final double MAGIC = 0.048f;
  // franch 7:48
  // private static final double MAGIC = 0.049f;
  private static double accToSpeed = 0.02040816f;
  // 2.0602751
  static double maxt = 1.0f;
  static double pt = 1.0f;
  static PrintWriter lastLog;
  CarPosition predicted;

  public Hinuri(BufferedReader reader, PrintWriter writer) throws IOException {
    super(reader, writer);
  }

  @Override
  protected void init() {
    try {
      lastLog = new PrintWriter(new FileWriter(LASTRUN_LOG), false);
    } catch (IOException e) {
      System.exit(-1);
    }
  }

  @Override
  protected void handleCrash() {
    lastLog.println("crash");
    System.out.println(ourLastCarPosition);
    System.out.println(ourCarPosition);
  }

  @Override
  protected void tournamentEnd() {
    super.tournamentEnd();
  }

  @Override
  protected void tick(String gameTick) {
    sendRouteSwitchIfNeeded();
    sendTurboIfNeeded(pt,gameTick);
    overTakeOrBlockIfNeeded();
    int tick = 0;
    try {
      tick = Integer.parseInt(gameTick);
    } catch (Exception e) {
    }
    if (tick == 4) {
      checkMagicNumber();
    }
    double throttle = 1f;
    CarPosition realNextCarPosition = getNextCarPosition(ourCarPosition, pt);
    if(track.isTurn(ourCarPosition.piecePosition.pieceIndex) &&  track.distanceToNextStrait(ourCarPosition)<DISTANCE_TO_STRAIT ) {
      throttle=1.0f;
    }
    else if (needBreak(realNextCarPosition) ){;//&& ourCarPosition.piecePosition.pieceIndex<38) {
      throttle = 0.0f;
    } else {
      CarPosition nextCarPosition = getNextCarPosition(ourCarPosition, 0f);
      double turboFactor = 1f;
      if (ourCarPosition.turbo.turboOn) {
        turboFactor = ourCarPosition.turbo.turboFactor;
      }
      if (nextCarPosition.piecePosition.pieceIndex != ourCarPosition.piecePosition.pieceIndex
          && track.isTurn(nextCarPosition.piecePosition.pieceIndex)) {
        throttle = getThrottleForSpeedChange(maxSpeedAtTurn(nextCarPosition), ourCarPosition.velocity, turboFactor);
      } else if (track.isTurn(ourCarPosition.piecePosition.pieceIndex)) {
        double maxSpeedAtTurn = maxSpeedAtTurn(ourCarPosition);

        if (ourCarPosition.velocity > maxSpeedAtTurn) {
          System.out.println(tick + ": " + ourCarPosition.velocity + " : Over speed limit: " + maxSpeedAtTurn
              + " at turn " + ourCarPosition.piecePosition);
          throttle = getThrottleForSpeedChange(maxSpeedAtTurn, ourCarPosition.velocity, turboFactor);
        } else {
          throttle = (ourCarPosition.velocity-2.0f*Math.abs(ourCarPosition.angleVelocity))/4.8f;
        }
      }
    }
    double add = Math.abs(ourCarPosition.angleVelocity) / 80f;
  /* if(
        track.distanceToNextStrait(ourCarPosition)<180.0 && (
        (track.segments.get(ourCarPosition.piecePosition.pieceIndex).angle<0.0 && ourCarPosition.angle>10.0) ||
        (track.segments.get(ourCarPosition.piecePosition.pieceIndex).angle>0.0 && ourCarPosition.angle<-10.0)) ) {
      throttle=1.0f;
      //llllllll ,,,,,,m,åknbbbbbbb mjbh
    }*/
    throttle = throttle ;//+ add;
    velosum+=ourCarPosition.velocity;
    velocount++;
    System.out.printf("[%4d|%2d|%1d] T[%2.3f] V[%2.2f|%2.2f][%2.2f] A[%2.2f|%2.2f]R[%2.1f] L[%1d|%1d] S[%2.2f|%2.2f]\n",
        tick, ourCarPosition.piecePosition.pieceIndex, ourCarPosition.piecePosition.lap,
        throttle,
        ourCarPosition.velocity,
        velosum/velocount,
        ourCarPosition.angleVelocity,
        ourCarPosition.angle,track.segments.get(ourCarPosition.piecePosition.pieceIndex).angle,track.segments.get(ourCarPosition.piecePosition.pieceIndex).radius,
        ourCarPosition.piecePosition.lane.startLaneIndex,ourCarPosition.piecePosition.lane.endLaneIndex,
        track.distanceToNextTurn(ourCarPosition),track.distanceToNextStrait(ourCarPosition));
    if (throttle > 1f)
      throttle = 1f;
    if (throttle < 0f)
      throttle = 0f;
    send(new Throttle(throttle));
    if (predicted != null
        && (ourCarPosition.piecePosition.lap != predicted.piecePosition.lap
            || Math.abs(ourCarPosition.piecePosition.inPieceDistance - predicted.piecePosition.inPieceDistance) > 0.001f
            || Math.abs(ourCarPosition.velocity - predicted.velocity) > 0.001f || Math.abs(ourCarPosition.acceleration
            - predicted.acceleration) > 0.001f)) {
      lastLog.println("**********************************************************************************");
    }
    predicted = getNextCarPosition(ourCarPosition, pt);
    logCarPosition("r" + tick, ourCarPosition, throttle);
    lastLog.println("---------------------------------------------------------------------------------");
    logCarPosition("p" + (tick + 1), predicted, pt);
    pt = throttle;
  }

  private boolean needBreak(CarPosition carPosition) {
    int nextTurn = track.nextTurn(carPosition);
    if (carPosition.piecePosition.lap == 2 && nextTurn < carPosition.piecePosition.pieceIndex) {
      return false;
    }
    return !isSafe(carPosition);
  }

  private void overTakeOrBlockIfNeeded() {
    for (CarPosition enemy : carPositions) {
      if (enemy.id.equals(ourCarPosition.id)) {
        continue;
      } else {
        // jos sama lane tai tulossa samalla
        if (enemy.piecePosition.lane.endLaneIndex == ourCarPosition.piecePosition.lane.endLaneIndex) {
          if (enemy.piecePosition.pieceIndex == ourCarPosition.piecePosition.pieceIndex)
            if (enemy.piecePosition.inPieceDistance + 0.2 > ourCarPosition.piecePosition.inPieceDistance) {
              // jos etäisyys lyhyt ja vihu hitaampi ja edesä tehdään switch
              if (Math.random() < 0.8) { // not always cos we so räikkönen
                send(new SwitchLane(ourCarPosition.piecePosition.lane.startLaneIndex == 0
                    || ourCarPosition.piecePosition.lane.startLaneIndex < track.lanes.length - 1 ? "Right" : "Left"));
              }
            }
        }
      }
    }
  }

  private boolean sendTurboIfNeeded(double throttle, String gametick) {
    CarPosition nextCarPosition = getNextCarPosition(ourCarPosition, pt);
    if (ourCarPosition.turbo.turboFactor > 0f && isSafeToActivateTurbo(nextCarPosition)
        && track.distanceToNextStrait(ourCarPosition)<DISTANCE_TO_STRAIT && getNextSectionOfType(SectionType.strait).nthLongest < 2
        && getNextSectionOfType(SectionType.strait).nthLongest > 0 && !ourCarPosition.turbo.turboOn) {
      send(new ActivateTurbo(gametick));
      ourCarPosition.turbo.turboOn = true;
      return true;
    }
    return false;
  }

  private boolean isSafeToActivateTurbo(CarPosition carPosition) {
    carPosition.turbo.turboOn = true;
    return isSafe(carPosition);
  }

  private void checkMagicNumber() {
    System.out.println("MN=" + ourCarPosition.acceleration / (10f * pt - ourCarPosition.velocity));
    accToSpeed = ourCarPosition.acceleration / (10f * pt - ourCarPosition.velocity);
  }

  private boolean isSafe(CarPosition nextCarPosition) {
    if (track.segments.get(nextCarPosition.piecePosition.pieceIndex) == null) {
      return true;
    }
    int ticksToCheck = 40;
    CarPosition positionToBeChecked = nextCarPosition;
    for (int i = 0; i < ticksToCheck; i++) {
      int pieceIndex = positionToBeChecked.piecePosition.pieceIndex;
      if (track.isTurn(pieceIndex) && positionToBeChecked.velocity > maxSpeedAtTurn(positionToBeChecked)) {
        return false;
      }
      positionToBeChecked = getNextCarPosition(positionToBeChecked, 0f);
    }
    return true;
  }

  private double maxSpeedAtTurn(CarPosition carPosition) {
    return Math.sqrt(track.radius(carPosition) * MAGIC * (dimensions.getAngularD()));
  }

  private double getAcceleration(double velocity, double throttle, double turboFactor) {
    return -velocity * accToSpeed + accToSpeed * 10f * throttle * turboFactor;
  }

  private double getNextSpeed(double velocity, double throttle, double turboFactor) {
    return (velocity + accToSpeed * 10f * throttle * turboFactor) / (1f + accToSpeed);
  }

  private double getThrottleForSpeedChange(double velocityNext, double velocity, double turboFactor) {
    return ((1f + accToSpeed) * velocityNext - velocity) / (accToSpeed * 10f * turboFactor);
  }

  private CarPosition getNextCarPosition(CarPosition currentPosition, double throttle) {
    CarPosition nextPosition = new CarPosition();
    double turboFactor = 1f;
    nextPosition.turbo = currentPosition.turbo.clone();
    if (currentPosition.turbo.turboOn) {
      nextPosition.turbo.turboTicksLeft--;
      if (nextPosition.turbo.turboTicksLeft <= 0) {
        nextPosition.turbo = new Turbo();
      }
    }
    if (nextPosition.turbo.turboOn) {
      turboFactor = nextPosition.turbo.turboFactor;
    }
    nextPosition.velocity = getNextSpeed(currentPosition.velocity, throttle, turboFactor);
    nextPosition.acceleration = getAcceleration(nextPosition.velocity, throttle, turboFactor);
    nextPosition.angle = getNextAngle(currentPosition, throttle);
    nextPosition.angleVelocity = getNextAngleVelocity(currentPosition, throttle);
    nextPosition.piecePosition = new CarPosition.Position();
    nextPosition.piecePosition.pieceIndex = currentPosition.piecePosition.pieceIndex;
    nextPosition.piecePosition.lap = currentPosition.piecePosition.lap;
    nextPosition.piecePosition.lane = new CarPosition.Lane();
    double nipd = nextPosition.velocity + currentPosition.piecePosition.inPieceDistance;
    double nipdd = nipd
        - track.segments.get(currentPosition.piecePosition.pieceIndex).lengths[currentPosition.piecePosition.lane.startLaneIndex];
    if (nipdd > 0) {
      nextPosition.piecePosition.pieceIndex++;
      nextPosition.piecePosition.lane.startLaneIndex = currentPosition.piecePosition.lane.endLaneIndex;
      nextPosition.piecePosition.inPieceDistance = nipdd;
      if (nextPosition.piecePosition.pieceIndex >= track.segments.size()) {
        nextPosition.piecePosition.pieceIndex = 0;
        nextPosition.piecePosition.lap++;
      }
    } else {
      nextPosition.piecePosition.lane.startLaneIndex = currentPosition.piecePosition.lane.startLaneIndex;
      nextPosition.piecePosition.inPieceDistance = nipd;
    }
    nextPosition.piecePosition.lane.endLaneIndex = nextPosition.piecePosition.lane.startLaneIndex;
    for (SwitchOn switchOn : route.switches) {
      if (switchOn.pieceId == nextPosition.piecePosition.pieceIndex) {
        nextPosition.piecePosition.lane.endLaneIndex = switchOn.toTrack;
        break;
      }
      if (switchOn.pieceId > nextPosition.piecePosition.pieceIndex) {
        break;
      }
    }
    return nextPosition;
  }

  private double getNextAngleVelocity(CarPosition currentPosition, double throttle) {
    // TODO Auto-generated method stub
    return 0;
  }

  private double getNextAngle(CarPosition currentPosition, double throttle) {
    // TODO Auto-generated method stub
    return 0;
  }

  private void logCarPosition(String gameTick, CarPosition carPosition, double throttle) {
    lastLog.print(gameTick);
    lastLog.print(",");
    lastLog.print(carPosition.piecePosition.lap);
    lastLog.print(",");
    lastLog.print(carPosition.piecePosition.pieceIndex);
    lastLog.print(",");
    lastLog.print(carPosition.piecePosition.lane.startLaneIndex);
    lastLog.print("->");
    lastLog.print(carPosition.piecePosition.lane.endLaneIndex);
    lastLog.print(",");
    lastLog.print(carPosition.piecePosition.inPieceDistance);
    lastLog.print(",");
    lastLog.print(carPosition.velocity);
    lastLog.print(",");
    lastLog.print(carPosition.acceleration);
    lastLog.print(",");
    lastLog.print(carPosition.angle);
    lastLog.print(",");
    lastLog.print(carPosition.angleVelocity);
    lastLog.print(",");
    lastLog.print(track.radius(carPosition));
    lastLog.print(",");
    lastLog.print(track.segments.get(carPosition.piecePosition.pieceIndex).angle);
    lastLog.print(",");
    lastLog.print(throttle);
    lastLog.print(",");
    lastLog.print(carPosition.turbo.turboOn);
    lastLog.print(",");
    lastLog.print(carPosition.turbo.turboTicksLeft);
    lastLog.print(",");
    lastLog.print(carPosition.turbo.turboFactor);
    lastLog.println();
  }
}
