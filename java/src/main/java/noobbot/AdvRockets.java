package noobbot;

import static java.nio.charset.StandardCharsets.UTF_8;
import static java.util.Arrays.asList;

import java.io.BufferedReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;

import msgs.CarPosition;

public class AdvRockets extends AbstractBot {
  public static final String LASTRUN_LOG = "Rockets_lastrun.log";
  private final int MAX_ANGLE = 50;
  private double RISK_FACTOR = 1.44;
  static PrintWriter lastLog;
  CarPosition predicted;

  public AdvRockets(BufferedReader reader, PrintWriter writer) throws IOException {
    super(reader, writer);
  }

  @Override
  protected void init() {
    try {
      lastLog = new PrintWriter(new FileWriter(LASTRUN_LOG), false);
    } catch (IOException e) {
      System.exit(-1);
    }
  }

  @Override
  protected void handleCrash() {
    lastLog.println("Crash");
    System.out.println("Crash");
    System.out.println(ourLastCarPosition);
    System.out.println(ourCarPosition);
  }

  @Override
  protected void tournamentEnd() {
    super.tournamentEnd();
  }

  @Override
  protected void tick(String gameTick) {
    int tick = 0;
    try {
      tick = Integer.parseInt(gameTick);
    } catch (Exception e) {
    }

    checkAngularMagicNumbers();

    CarPosition realNextCarPosition = ourCarPosition;
    double throttle = findMaxThrottle(realNextCarPosition);
    if (track.isTurn(realNextCarPosition.piecePosition.pieceIndex) && !hasBiggerTurnComing(realNextCarPosition)) {
      double maxSpeedAtTurnWithoutSlip = maxSpeedAtTurnWithoutSlip(realNextCarPosition);
      double maxSpeedAtTurn = maxSpeedAtTurn(realNextCarPosition);
      if (ourCarPosition.angle * ourCarPosition.angleVelocity < 0) {
        throttle = throttle + ourCarPosition.angle / 35;
      } else {
        double absAngle = Math.abs(ourCarPosition.angle);
        if (absAngle > getMaxAngle()) {
          throttle = 0;
        } else {
          throttle = getThrottleForSpeedChange(maxSpeedAtTurnWithoutSlip + (getMaxAngle() - absAngle)
              * (maxSpeedAtTurn - maxSpeedAtTurnWithoutSlip) / getMaxAngle(), realNextCarPosition.velocity, 1);
        }
      }
    }

    if (tick > 4
        && predicted != null
        && (ourCarPosition.piecePosition.lap != predicted.piecePosition.lap
            || Math.abs(ourCarPosition.piecePosition.inPieceDistance - predicted.piecePosition.inPieceDistance) > 0.001
            || Math.abs(ourCarPosition.velocity - predicted.velocity) > 0.001 || Math.abs(ourCarPosition.acceleration
            - predicted.acceleration) > 0.001)) {
      lastLog.println("**********************************************************************************");
    }
    predicted = getNextCarPosition(ourCarPosition, prevThrottle);
    logCarPosition("r" + tick, ourCarPosition, throttle);
    lastLog.println("---------------------------------------------------------------------------------");
    logCarPosition("p" + (tick + 1), predicted, prevThrottle);

    if (throttle > 1)
      throttle = 1;
    if (throttle < 0)
      throttle = 0;
    if (tick < 4)
      throttle = 1;

    boolean commandSent = false;
    if (checkMagicNumbers(tick)) {
      commandSent = true;
    } else if (sendRouteSwitchIfNeeded()) {
      commandSent = true;
    } else if (sendTurboIfNeeded(prevThrottle)) {
      commandSent = true;
    }

    if (!commandSent) {
      send(new Throttle(throttle));
      prevThrottle = throttle;
    }
  }

  private double getMaxAngle() {
    return MAX_ANGLE - (C_UG - 0.35) * 70 - (maxSpeed - 10);
  }

  private boolean hasBiggerTurnComing(CarPosition realNextCarPosition) {
    CarPosition nextCarPosition = getNextCarPosition(realNextCarPosition, 1);
    int ticksToCheck = 3 + (int) ((maxSpeed - 7) / 2);
    for (int i = 0; i < ticksToCheck; i++) {
      nextCarPosition = getNextCarPosition(nextCarPosition, 1);
    }
    if (nextCarPosition.piecePosition.pieceIndex == realNextCarPosition.piecePosition.pieceIndex) {
      return false;
    }
    if (track.isTurn(nextCarPosition.piecePosition.pieceIndex)) {
      return track.radius(realNextCarPosition) > track.radius(nextCarPosition);
    }
    return false;
  }

  private double findMaxThrottle(CarPosition carPosition) {
    double ttmax = maxt;
    double ttmin = 0f;
    double tt = ttmax;
    int maxTry = 5;
    while (maxTry > 0) {
      CarPosition nextCarPosition = getNextCarPosition(carPosition, tt);
      maxTry--;
      if (isSafe(nextCarPosition)) {
        if (tt == ttmax) {
          return ttmax;
        }
        ttmin = tt;
      } else {
        ttmax = tt;
      }
      tt = (ttmin + ttmax) / 2;
    }
    return ttmin;
  }

  private boolean isSafe(CarPosition nextCarPosition) {
    if (track.segments.get(nextCarPosition.piecePosition.pieceIndex) == null) {
      return true;
    }
    int ticksToCheck = 38 + (int) ((maxSpeed - 7) * 1.5);
    CarPosition positionToBeChecked = nextCarPosition;
    for (int i = 0; i < ticksToCheck; i++) {
      int pieceIndex = positionToBeChecked.piecePosition.pieceIndex;
      if (track.isTurn(pieceIndex) && positionToBeChecked.velocity > maxSpeedAtTurn(positionToBeChecked)) {
        return false;
      }
      positionToBeChecked = getNextCarPosition(positionToBeChecked, 0);
    }
    return true;
  }

  @Override
  protected double maxSpeedAtTurn(CarPosition carPosition) {
    double mspeed = Math.sqrt(track.radius(carPosition) * C_UG * RISK_FACTOR);
    return mspeed;
  }

  protected double maxSpeedAtTurnWithoutSlip(CarPosition carPosition) {
    double mspeed = Math.sqrt(track.radius(carPosition) * C_UG / 1.5 * RISK_FACTOR);
    return mspeed;
  }

  private void logCarPosition(String gameTick, CarPosition carPosition, double throttle) {
    lastLog.print(gameTick);
    lastLog.print(",");
    lastLog.print(carPosition.piecePosition.lap);
    lastLog.print(",");
    lastLog.print(carPosition.piecePosition.pieceIndex);
    lastLog.print(",");
    lastLog.print(carPosition.piecePosition.lane.startLaneIndex);
    lastLog.print("->");
    lastLog.print(carPosition.piecePosition.lane.endLaneIndex);
    lastLog.print(",");
    lastLog.print(carPosition.piecePosition.inPieceDistance);
    lastLog.print(",");
    lastLog.print(carPosition.velocity);
    lastLog.print(",");
    lastLog.print(carPosition.acceleration);
    lastLog.print(",");
    lastLog.print(carPosition.angle);
    lastLog.print(",");
    lastLog.print(carPosition.angleVelocity);
    lastLog.print(",");
    lastLog.print(carPosition.ticksForSlipAngle);
    lastLog.print(",");
    lastLog.print(track.radius(carPosition));
    lastLog.print(",");
    lastLog.print(track.segments.get(carPosition.piecePosition.pieceIndex).angle);
    lastLog.print(",");
    lastLog.print(throttle);
    lastLog.print(",");
    lastLog.print(carPosition.turbo.turboOn);
    lastLog.print(",");
    lastLog.print(carPosition.turbo.turboTicksLeft);
    lastLog.print(",");
    lastLog.print(carPosition.turbo.turboFactor);
    lastLog.println();
  }

  @SuppressWarnings("unchecked")
  public static void main(String... args) throws Exception {
    String host = args[0];
    int port = Integer.parseInt(args[1]);
    String botName = args[2];
    String botKey = args[3];
    List<String> trackNames = asList("keimola", "germany", "usa", "france", "suzuka", "imola", "england", "elaeintarha", "pentag");
    List<Thread> threads = new ArrayList<>();

    for (String trackName : trackNames) {
      Class<Main> botClass = (Class<Main>) Class.forName("noobbot." + botName);

      @SuppressWarnings("resource")
      Socket socket = new Socket();
      socket.connect(new InetSocketAddress(host, port), 120 * 1000);
      socket.setTcpNoDelay(true);
      socket.setSoTimeout(60 * 1000);
      final PrintWriter writer = new PrintWriter(new OutputStreamWriter(socket.getOutputStream(), UTF_8));
      final BufferedReader reader = new BufferedReader(new InputStreamReader(socket.getInputStream(), UTF_8));

      final Main bot = botClass.getConstructor(BufferedReader.class, PrintWriter.class).newInstance(reader, writer);
      try {
        bot.send(new JoinRace(botName, botKey, trackName, null, 1));
        Thread t = new Thread("track:" + trackName) {
          @Override
          public void run() {
            bot.safeRun();
          };
        };
        threads.add(t);
        t.start();
      } catch (Throwable t) {
        t.printStackTrace();
        t.printStackTrace(bot.log);
      }
    }
    for (Thread t : threads) {
      t.join();
    }
  }
}
