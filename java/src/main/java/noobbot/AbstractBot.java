package noobbot;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;

import msgs.CarPosition;
import msgs.Section;
import msgs.CarPosition.Position;
import msgs.Section.SectionType;
import msgs.TrackRoute.SwitchOn;
import msgs.Turbo;

public abstract class AbstractBot extends Main {
  private double damping = -0.0416535;
  private double amplitude = 0;
  private double period = 0;
  protected double accToSpeed = 0.02040816f;
  protected double maxSpeed = 10f;
  static double maxt = 1.0f;
  double prevThrottle = 1.0f;
  double prevSlipAngle =0.0;
  protected double ANGLE_SAFE_LIMES = 58.00;
  protected double C_UG = 0.35; // initial quess for staticfriction times
                                       // gravity constant related to critical
                                       // speed
  protected boolean checkUG = true; // flag used to update ug

  public AbstractBot(BufferedReader reader, PrintWriter writer) throws IOException {
    super(reader, writer);
  }

  protected boolean needBreak(CarPosition carPosition) {
    return !isSafe(carPosition);
  }

  protected void overTakeOrBlockIfNeeded( String gametick ) {
    for (CarPosition enemy : carPositions) {
      if (enemy.id.equals(ourCarPosition.id)) {
        continue;
      } else {
        // jos sama lane tai tulossa samalla
        if (enemy.piecePosition.lane.endLaneIndex == ourCarPosition.piecePosition.lane.endLaneIndex) {
          if (enemy.piecePosition.pieceIndex == ourCarPosition.piecePosition.pieceIndex)
            if (enemy.piecePosition.inPieceDistance > ourCarPosition.piecePosition.inPieceDistance) {
              if (Math.random() < 0.99) { // not always cos we so räikkönen
                send(new SwitchLane(ourCarPosition.piecePosition.lane.startLaneIndex == 0
                    || ourCarPosition.piecePosition.lane.startLaneIndex < track.lanes.length - 1 ? "Right" : "Left"));
              }
            }
        }
      }
    }
  }


  protected void overTakeOrBlockIfNeeded() {
    for (CarPosition enemy : carPositions) {
      if (enemy.id.equals(ourCarPosition.id)) {
        continue;
      } else {
        // jos sama lane tai tulossa samalla
        if (enemy.piecePosition.lane.endLaneIndex == ourCarPosition.piecePosition.lane.endLaneIndex) {
          if (enemy.piecePosition.pieceIndex == ourCarPosition.piecePosition.pieceIndex)
            if (enemy.piecePosition.inPieceDistance + 0.2 > ourCarPosition.piecePosition.inPieceDistance) {
              // jos etäisyys lyhyt ja vihu hitaampi ja edesä tehdään switch
              if (Math.random() < 0.8) { // not always cos we so räikkönen
                send(new SwitchLane(ourCarPosition.piecePosition.lane.startLaneIndex == 0
                    || ourCarPosition.piecePosition.lane.startLaneIndex < track.lanes.length - 1 ? "Right" : "Left"));
              }
            }
        }
      }
    }
  }

  protected boolean sendTurboIfNeeded(double throttle) {
    CarPosition nextCarPosition = getNextCarPosition(ourCarPosition, throttle);
    if (ourCarPosition.turbo.turboFactor > 0f
        //&& !track.isTurn(nextCarPosition.piecePosition.pieceIndex)
        && (!track.isTurn(nextCarPosition.piecePosition.pieceIndex) || (track.distanceToNextStrait(ourCarPosition) < (track.radius(ourCarPosition) * 0.5)))
        && isSafeToActivateTurbo(nextCarPosition) && getNextSectionOfType(SectionType.strait).nthLongest < 2
        && getNextSectionOfType(SectionType.strait).nthLongest > 0 && !ourCarPosition.turbo.turboOn) {
      send(new ActivateTurbo());
      ourCarPosition.turbo.turboOn = true;
      return true;
    }
    return false;
  }

  protected boolean isSafeToActivateTurbo(CarPosition carPosition) {
    carPosition.turbo.turboOn = true;
    return isSafe(carPosition);
  }

  protected boolean checkMagicNumbers(int tick) {
    if (tick < 4) {
      send(new Throttle(1.0));
      prevThrottle = 1;
      return true;
    }
    if (tick == 4) {
      // assume no change in throttle for first 3 tick
      accToSpeed = (ourCarPosition.acceleration - ourLastCarPosition.acceleration)
          / (ourLastCarPosition.velocity - ourCarPosition.velocity);
      maxSpeed = (ourCarPosition.acceleration / accToSpeed + ourCarPosition.velocity) / prevThrottle;
      System.out.println("MN=" + accToSpeed + ":" + maxSpeed);
    }
    return false;
  }

  //returns real values not risky ones
  protected void checkAngularMagicNumbers() {
    if (track.isTurn(ourCarPosition.piecePosition.pieceIndex) && checkUG && Math.abs(ourCarPosition.angle) > 0.0) {
      double degrees2radians = Math.abs(ourCarPosition.angle) * Math.PI * 2.0 / 360.0;
      double prettyMagicRatio = 160.0 * degrees2radians;
      double safeVelocity = ourCarPosition.velocity - prettyMagicRatio;
      C_UG = (C_UG+(Math.pow(safeVelocity, 2) / track.radius(ourCarPosition)))*0.5;
      checkUG = false;
      System.out.println("\n Checked muu:" + C_UG + " slip=" + degrees2radians + " " + ourCarPosition.angle + " "
          + prettyMagicRatio + " " + safeVelocity);
    }
  }

  protected boolean checkAngleIsSafe(CarPosition nextCarPosition) {
    if (track.segments.get(nextCarPosition.piecePosition.pieceIndex) == null) {
      return true;
    }
    int ticksToCheck = 40;
    CarPosition positionToBeChecked = nextCarPosition;
    for (int i = 0; i < ticksToCheck; i++) {
      if(Math.abs(nextCarPosition.angle)>ANGLE_SAFE_LIMES && checkPendulumSafe(nextCarPosition)) {
        return false;
      }
      positionToBeChecked = getNextCarPosition(positionToBeChecked, 0f);
    }
    return true;
  }

  private boolean isSafe(CarPosition nextCarPosition) {
    if (track.segments.get(nextCarPosition.piecePosition.pieceIndex) == null) {
      return true;
    }
    int ticksToCheck = 40;
    CarPosition positionToBeChecked = nextCarPosition;
    for (int i = 0; i < ticksToCheck; i++) {
      int pieceIndex = positionToBeChecked.piecePosition.pieceIndex;
      if(track.isTurn(pieceIndex) && positionToBeChecked.velocity > maxSpeedAtTurn(positionToBeChecked)) {
        return false;
      }
      positionToBeChecked = getNextCarPosition(positionToBeChecked, 0f);
    }
    return true;
  }

  protected abstract double maxSpeedAtTurn(CarPosition carPosition);

  protected double getAcceleration(double velocity, double throttle, double turboFactor) {
    return -velocity * accToSpeed + accToSpeed * maxSpeed * throttle * turboFactor;
  }

  protected double getNextSpeed(double velocity, double throttle, double turboFactor) {
    return (velocity + accToSpeed * maxSpeed * throttle * turboFactor) / (1 + accToSpeed);
  }

  protected double getThrottleForSpeedChange(double velocityNext, double velocity, double turboFactor) {
    return ((1f + accToSpeed) * velocityNext - velocity) / (accToSpeed * maxSpeed * turboFactor);
  }

  protected CarPosition getNextCarPosition(CarPosition currentPosition, double throttle) {
    CarPosition nextPosition = new CarPosition();
    double turboFactor = 1f;
    nextPosition.turbo = currentPosition.turbo.clone();
    if (nextPosition.turbo.turboOn) {
      if (nextPosition.turbo.turboDurationTicks > nextPosition.turbo.turboTicksLeft + 1) {
        turboFactor = nextPosition.turbo.turboFactor;
      }
      nextPosition.turbo.turboTicksLeft--;
      if (nextPosition.turbo.turboTicksLeft < 0) {
        nextPosition.turbo = new Turbo();
      }
    }
    nextPosition.velocity = getNextSpeed(currentPosition.velocity, throttle, turboFactor);
    nextPosition.acceleration = getAcceleration(nextPosition.velocity, throttle, turboFactor);
    nextPosition.piecePosition = new CarPosition.Position();
    nextPosition.piecePosition.pieceIndex = currentPosition.piecePosition.pieceIndex;
    nextPosition.piecePosition.lap = currentPosition.piecePosition.lap;
    nextPosition.piecePosition.lane = new CarPosition.Lane();
    double nipd = nextPosition.velocity + currentPosition.piecePosition.inPieceDistance;
    double nipdd = nipd
        - track.segments.get(currentPosition.piecePosition.pieceIndex).switchLengths[currentPosition.piecePosition.lane.startLaneIndex][currentPosition.piecePosition.lane.endLaneIndex];
    if (nipdd > 0) {
      nextPosition.piecePosition.pieceIndex++;
      nextPosition.piecePosition.lane.startLaneIndex = currentPosition.piecePosition.lane.endLaneIndex;
      nextPosition.piecePosition.inPieceDistance = nipdd;
      if (nextPosition.piecePosition.pieceIndex >= track.segments.size()) {
        nextPosition.piecePosition.pieceIndex = 0;
        nextPosition.piecePosition.lap++;
      }
    } else {
      nextPosition.piecePosition.lane.startLaneIndex = currentPosition.piecePosition.lane.startLaneIndex;
      nextPosition.piecePosition.inPieceDistance = nipd;
    }
    nextPosition.piecePosition.lane.endLaneIndex = nextPosition.piecePosition.lane.startLaneIndex;
    for (SwitchOn switchOn : route.switches) {
      if (switchOn.pieceId == nextPosition.piecePosition.pieceIndex) {
        nextPosition.piecePosition.lane.endLaneIndex = switchOn.toTrack;
        break;
      }
      if (switchOn.pieceId > nextPosition.piecePosition.pieceIndex) {
        break;
      }
    }
    for (Section s : route.sections) {
      Position currentPiece = nextPosition.piecePosition;
      if (s.firstPiece <= currentPiece.pieceIndex && currentPiece.pieceIndex <= s.lastPiece) {
        nextPosition.section = s;
        break;
      }
    }
    if (nextPosition.section.firstPiece == currentPosition.section.firstPiece && currentPosition.angle > 0
        && currentPosition.ticksForSlipAngle > 0 && amplitude > 0 && period > 0) {
      nextPosition.ticksForSlipAngle = currentPosition.ticksForSlipAngle + 1;
      nextPosition.angleVelocity = amplitude * Math.exp(nextPosition.ticksForSlipAngle * damping)
          * Math.sin(nextPosition.ticksForSlipAngle * period);
     // System.out.println(nextPosition.ticksForSlipAngle + ":" + nextPosition.angleVelocity + ":" + amplitude + ":" + damping + ":" + period);
      nextPosition.angle = currentPosition.angle + nextPosition.angleVelocity;
    }
    return nextPosition;
  }

  protected void setSlipAngleConstant() {
    if (ourCarPosition.ticksForSlipAngle == 2) {
      period = Math.acos(ourCarPosition.angleVelocity / ourLastCarPosition.angleVelocity / Math.exp(damping) / 2);
      amplitude = ourCarPosition.angleVelocity / Math.sin(period * 2) / Math.exp(damping * 2);
      System.out.println("amplitude=" + amplitude + "  period=" + period);
    }
  }

  protected boolean checkPendulumSafe(CarPosition carPosition) {
    double r = track.radius(carPosition);
    double beta = carPosition.angle;
    double ypsilon = track.segments.get(carPosition.piecePosition.pieceIndex).angle;
    double distance2strait = track.distanceToNextStrait(carPosition);
    boolean sameAsBefore = sameSign(beta,prevSlipAngle);
    boolean sameAsRoute = sameSign(ypsilon,beta);
    boolean magnitudeScary = Math.abs(beta)>ANGLE_SAFE_LIMES;

    if( sameAsBefore && !sameAsRoute ) { //pendulum going same as previously and that is opposite to turn angle
      return true;
    }
    else if( sameAsBefore && sameAsRoute && magnitudeScary ) {
      // pendulum going towards angle and angle close
      return false;
    }
    else if( sameAsBefore && sameAsRoute && distance2strait>(0.5*r) ) {
      //pendulum going towards angle distance to straigh longer than half radius
      return false;
    }
    else if( sameAsBefore && sameAsRoute && distance2strait>(0.25*r) && carPosition.turbo.turboFactor>1.0) {
      //pendulum going towards angle distance to straigh longer than quarter of radius but turbo factor exist and turbo on
      return false;
    }
    else {
      return true;
    }
   }

  private boolean sameSign(double a, double b) {
    return (a<0.0&&b<0.0)||(a>0.0&&b>0.0);
  }
}
