package noobbot;

import static java.lang.Boolean.FALSE;
import static java.lang.Math.PI;
import static java.lang.Math.abs;
import static java.lang.Math.cos;
import static java.lang.Math.max;
import static java.lang.Math.min;
import static java.lang.Math.sqrt;
import static java.lang.System.currentTimeMillis;
import static java.lang.System.err;
import static java.lang.System.out;
import static java.lang.Thread.sleep;
import static java.nio.charset.StandardCharsets.UTF_8;
import static java.util.Arrays.asList;
import static java.util.Arrays.fill;
import static msgs.Section.SectionType.strait;

import java.io.BufferedReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;

import msgs.CarPosition;
import msgs.CarPosition.Position;
import msgs.Dimensions;
import msgs.Id;
import msgs.Race;
import msgs.Section;
import msgs.Track;
import msgs.Track.Lane;
import msgs.Track.Segment;
import msgs.TrackRoute;
import msgs.TrackRoute.SwitchOn;
import msgs.Turbo;

import com.google.common.base.Joiner;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

public abstract class Main {
  PrintWriter log;
  static String trackName;

  @SuppressWarnings("unchecked")
  public static void main(String... args) throws Exception {
    String host = args[0];
    int port = Integer.parseInt(args[1]);
    String botName = args[2];
    String botKey = args[3];
    trackName = "keimola";
    if (args.length > 4) {
      trackName = args[4];
    }
    if ("random".equals(trackName)) {
      List<String> trackNames = asList("keimola", "germany", "usa", "france", "suzuka", "imola", "england", "elaeintarha");
      trackName = trackNames.get(new Random().nextInt(trackNames.size()));
    }
    String password = "oeaoea" + currentTimeMillis();
    if (args.length > 5) {
      password = args[5];
    }

    System.out.println("Connecting to " + host + ":" + port + " as " + botName + "/" + botKey + " track uff " + trackName);

    String classNames[] = botName.split(",");
    if ("OEA".equals(botName)) {
      classNames = new String[] { AdvRockets2.class.getSimpleName() };
    }
    List<Thread> threads = new ArrayList<>();

    boolean raceCreated = false;
    if (botName.indexOf("-") > 0) {
      raceCreated = true;
      password = null;
    }
    for (String className : classNames) {
      if (className.equals("-")) {
        continue;
      }
      Class<Main> botClass = (Class<Main>) Class.forName("noobbot." + className);

      @SuppressWarnings("resource")
      Socket socket = new Socket();
      socket.connect(new InetSocketAddress(host, port), 120 * 1000);
      socket.setTcpNoDelay(true);
      socket.setSoTimeout(60 * 1000);
      final PrintWriter writer = new PrintWriter(new OutputStreamWriter(socket.getOutputStream(), UTF_8));
      final BufferedReader reader = new BufferedReader(new InputStreamReader(socket.getInputStream(), UTF_8));

      final Main bot = botClass.getConstructor(BufferedReader.class, PrintWriter.class).newInstance(reader, writer);
      try {
        if ("OEA".equals(botName)) {
          bot.send(new Join(botName, botKey));
        } else if (classNames.length == 1) {
          bot.send(new JoinRace(botName, botKey, trackName, null, 1));
        } else if (raceCreated) {
          bot.send(new JoinRace(className, botKey, trackName, password, classNames.length));
        } else {
          bot.send(new CreateRace(className, botKey, trackName, password, classNames.length));
          raceCreated = true;
          // let new race be replicated between machines
          sleep(500);
        }
        Thread t = new Thread("BOT:" + className) {
          @Override
          public void run() {
            bot.safeRun();
          };
        };
        threads.add(t);
        t.start();
      } catch (Throwable t) {
        t.printStackTrace();
        t.printStackTrace(bot.log);
      }
    }
    for (Thread t : threads) {
      t.join();
    }
  }

  final Gson gson = new Gson();
  PrintWriter writer;
  BufferedReader reader;
  Track track;
  TrackRoute lastRoute;
  TrackRoute route;
  CarPosition[] lastCarPositions;
  CarPosition[] carPositions;
  CarPosition ourLastCarPosition;
  CarPosition ourCarPosition;
  String ourCarColor;
  Dimensions dimensions;
  Date inTime;

  // for route optimizer code
  TreeSet<TrackRoute> routes;
  TrackRoute shortestRoute;

  public Main(final BufferedReader reader, final PrintWriter writer) {
    this.reader = reader;
    this.writer = writer;
    try {
      log = new PrintWriter(new FileWriter(this.getClass().getSimpleName() + "_" + trackName + "_"
          + new SimpleDateFormat("YYYYMMdd-HHmmss.SSS").format(new Date()) + ".log"), false);
    } catch (IOException e) {
      throw new RuntimeException(e);
    }
  }

  protected void safeRun() {
    try {
      run();
    } catch (Throwable t) {
      t.printStackTrace(log);
      t.printStackTrace();
    } finally {
      log.close();
    }
  }

  protected void run() throws Exception {
    final SimpleDateFormat df = new SimpleDateFormat("YYYYMMdd-HHmmss.SSS");
    String line;
    boolean correctEnd = false;
    Set<String> unknownCommands = new HashSet<>();
    log.flush();
    while ((line = reader.readLine()) != null) {
      inTime = new Date();
      log.println(df.format(inTime) + " > " + line);
      final MsgWrapper msgFromServer = gson.fromJson(line, MsgWrapper.class);
      try {
        switch (msgFromServer.msgType) {
        case "carPositions":
          parsePositions(msgFromServer);
          tick(msgFromServer.gameTick);
          break;
        case "turboAvailable":
          ourCarPosition.turbo = msgFromServer.getData(Turbo.class);
          ourCarPosition.turbo.turboTicksLeft = ourCarPosition.turbo.turboDurationTicks;
          System.out.println("turboAvailable=" + ourCarPosition.turbo.turboFactor + " for "
              + ourCarPosition.turbo.turboDurationTicks + " ticks");
          break;
        case "turboStart":
          turboStart(msgFromServer.getData(Id.class), msgFromServer.gameTick);
          break;
        case "turboEnd":
          turboEnd(msgFromServer.getData(Id.class), msgFromServer.gameTick);
          break;
        case "join":
          System.out.println("Joined");
          break;
        case "joinRace":
          System.out.println("Joined Race");
          break;
        case "gameInit":
          handleInit(msgFromServer);
          init();
          System.gc();
          break;
        case "crash":
          crash(msgFromServer.getData(Id.class));
          break;
        case "spawn":
          spawn(msgFromServer.getData(Id.class));
          break;
        case "lapFinished":
          System.out.println("lap finished with " + line);
          send(new Ping());
          lapFinished(msgFromServer.data.getAsJsonObject().getAsJsonObject("car").getAsJsonPrimitive("color").getAsString());
          break;
        case "finish":
          send(new Ping());
          finish(msgFromServer.getData(Id.class));
          break;
        case "gameEnd":
          System.out.println("Race end " + line);
          resetStateBetweenRaces();
          correctEnd = true;
          send(new Ping());
          System.gc();
          break;
        case "gameStart":
          send(new Throttle(1.0));
          System.out.println("Race start");
          correctEnd = false;
          System.gc();
          break;
        case "yourCar":
          ourCarColor = msgFromServer.data.getAsJsonObject().getAsJsonPrimitive("color").getAsString();
          System.out.println("I am " + ourCarColor);
          break;
        case "tournamentEnd":
          tournamentEnd();
          send(new Ping());
          System.gc();
          break;
        case "createRace":
          send(new Ping());
          break;
        default:
          if (unknownCommands.add(msgFromServer.msgType)) {
            err.println("Unknown message: " + line);
          }
          send(new Ping());
        }
      } catch (Throwable t) {
        t.printStackTrace();
        send(new Ping());
      }
    }
    if (!correctEnd) {
      System.out.println("Server unexpectedly closed socket :(");
      log.println("Server unexpectedly closed socket :(");
    }
    if (!unknownCommands.isEmpty()) {
      log.println("Unknown message types: " + unknownCommands);
      err.println("Unknown message types: " + unknownCommands);
    }
  }

  protected void lapFinished(String color) {
    for (CarPosition car : carPositions) {
      if (car.id.color.equals(color)) {
        if (car == ourCarPosition) {
          handleLapFinished();
        }
      }
    }
  }

  protected void handleLapFinished() {
  }

  private void turboEnd(Id id, String gameTick) {
    for (CarPosition car : carPositions) {
      if (car.id.color.equals(id.color)) {
        car.turbo.turboOn = false;
        car.turbo = new Turbo();
        if (car == ourCarPosition) {
          System.out.println("turboStop at " + gameTick);
        }
      }
    }
  }

  private void turboStart(Id id, String gameTick) {
    for (CarPosition car : carPositions) {
      if (car.id.color.equals(id.color)) {
        car.turbo.turboOn = true;
        if (car == ourCarPosition) {
          System.out.println("turboStart at " + gameTick);
        }
      }
    }
  }

  protected void tournamentEnd() {
    System.out.println("Tournament finished");
  }

  private void crash(Id id) {
    for (CarPosition car : carPositions) {
      if (car.id.color.equals(id.color)) {
        car.turbo.turboOn = false;
        car.turbo.turboTicksLeft = 0;
        car.alive = false;
        if (car == ourCarPosition) {
          handleCrash();
        }
      }
    }
  }

  protected void handleCrash() {

  }

  private void finish(Id id) {
    for (CarPosition car : carPositions) {
      if (car.id.color.equals(id.color)) {
        car.alive = false;
      }
    }
  }

  protected void spawn(Id id) {
    for (CarPosition car : carPositions) {
      if (car.id.color.equals(id.color)) {
        car.alive = true;
        if (car == ourCarPosition) {
          System.out.println("Spawn");
          route = getOptimalRoute(car);
        }
      }
    }
  }

  int lastRouteSentPiece;

  public boolean sendRouteSwitchIfNeeded() {
    detectCollision();
    Position pos = ourCarPosition.piecePosition;
    int nextPieceIndex = track.nextSegment(pos.pieceIndex);
    if (pos.pieceIndex != lastRouteSentPiece && track.hasSwitch(nextPieceIndex) && track.distanceTo(ourCarPosition, nextPieceIndex) < ourCarPosition.velocity * 2.5) {
      lastRouteSentPiece = pos.pieceIndex;
      int wantedLane = pos.lane.endLaneIndex;
      int switchCommand = route.getSwitchCommand(nextPieceIndex);
      int nextNextSwitchPieceIndex = track.nextSwitch(track.nextSegment(nextPieceIndex));
      wantedLane += switchCommand;
      double distBetweenSwitches = track.distanceTo(nextPieceIndex, 0, nextNextSwitchPieceIndex, wantedLane, 0);
      Map<Integer, CarPosition> nearestCarOnLane = new HashMap<>();
      for (CarPosition opponent : carPositions) {
        if (opponent == ourCarPosition || opponent.alive == FALSE) continue;
        Position opponentPos = opponent.piecePosition;
        if (opponentPos.pieceIndex == pos.pieceIndex && opponentPos.inPieceDistance > pos.inPieceDistance) {
          // opponent between us and the switch
          if (opponent.bumpedTime > 0) {
            // we have bumped to it lately
            CarPosition prevCar = nearestCarOnLane.get(opponentPos.lane.endLaneIndex);
            if (prevCar == null || prevCar.piecePosition.isAheadOf(opponentPos)) {
              nearestCarOnLane.put(wantedLane, opponent);
            }
          }
          if (opponent.isLeadingUs) {
            // it ahead of us in the race -> hopefully we could turbo it away from the track
          } else {
            // slower opponent ahead of us, assume it is using the optimal lane
            nearestCarOnLane.put(wantedLane, opponent);
          }
          continue;
        }
        for (int idx = nextPieceIndex; idx != nextNextSwitchPieceIndex; idx = track.nextSegment(idx)) {
          if (opponentPos.pieceIndex == idx) {
            // opponent in the next part of track between switches
            CarPosition prevCar = nearestCarOnLane.get(opponentPos.lane.endLaneIndex);
            if (prevCar == null || prevCar.piecePosition.isAheadOf(opponentPos)) {
              nearestCarOnLane.put(opponentPos.lane.endLaneIndex, opponent);
            }
            break;
          }
        }
      }
      for (Iterator<CarPosition> nearest = nearestCarOnLane.values().iterator(); nearest.hasNext(); ) {
        CarPosition opponent = nearest.next();
        if (opponent.bumpedTime > 0) {
          continue;
        }
        if (!isTooNear(opponent, nextNextSwitchPieceIndex)) {
          log.println("Far enough " + opponent.id.name);
          nearest.remove();
        }
      }
      if (nearestCarOnLane.get(wantedLane) != null) {
        int mostFreeSpaceLane = wantedLane;
        double bestDistToCar = 0;
        int leastBumpsLane = wantedLane;
        int leastBumpsToCar = 1000000;
        boolean useDist = false;
        for (int lane = max(wantedLane - 1, 0); lane < min(wantedLane + 1, track.lanes.length); ++lane) {
          CarPosition opponent = nearestCarOnLane.get(lane);
          if (opponent == null) {
            mostFreeSpaceLane = lane;
            bestDistToCar = distBetweenSwitches;
            leastBumpsLane = lane;
            leastBumpsToCar = 0;
          } else {
            double distToCar = distBetweenSwitches - track.distanceTo(opponent, nextNextSwitchPieceIndex);
            if (distToCar > bestDistToCar) {
              mostFreeSpaceLane = lane;
              bestDistToCar = distToCar;
            }
            if (max(opponent.bumpedTime, 0) == leastBumpsToCar) {
              // bumps are not helping
              useDist = true;
            }
            if (opponent.bumpedTime < leastBumpsToCar) {
              leastBumpsToCar = max(opponent.bumpedTime, 0);
              leastBumpsLane = lane;
            }
          }
        }

        log.println("Traffic on our wanted lane " + wantedLane + " using last bump time to choose best lane: " + !useDist);
        out.println("Traffic on our wanted lane " + wantedLane + " using last bump time to choose best lane: " + !useDist);
        if (useDist) {
          switchCommand = mostFreeSpaceLane - pos.lane.endLaneIndex;
        } else {
          switchCommand = leastBumpsLane - pos.lane.endLaneIndex;
        }
      }
      int actualLane = pos.lane.endLaneIndex + switchCommand;
      if (actualLane != wantedLane) {
        log.println("Deviating from optimal route: to lane " + (pos.lane.endLaneIndex + switchCommand) + ", wanted: " + wantedLane);
        out.println("Deviating from optimal route: to lane " + (pos.lane.endLaneIndex + switchCommand) + ", wanted: " + wantedLane);
      }
      if (switchCommand != 0) {
        send(new SwitchLane(switchCommand));
        return true;
      }
    }
    return false;
  }

  private void detectCollision() {
    if (ourLastCarPosition == null) {
      return;
    }
    if (ourCarPosition.velocity < ourLastCarPosition.velocity / 1.5) {
      double bumpedDist = 100000.0;
      CarPosition bumpedTo = null;
      for (CarPosition opp : carPositions) {
        if (opp.alive == FALSE || opp == ourCarPosition) continue;
        if (opp.piecePosition.lane.startLaneIndex == ourCarPosition.piecePosition.lane.startLaneIndex && opp.piecePosition.lane.endLaneIndex == ourCarPosition.piecePosition.lane.endLaneIndex && opp.piecePosition.pieceIndex == ourCarPosition.piecePosition.pieceIndex && opp.piecePosition.inPieceDistance > ourCarPosition.piecePosition.inPieceDistance) {
          if (opp.piecePosition.inPieceDistance < bumpedDist) {
            bumpedTo = opp;
            bumpedDist = opp.piecePosition.inPieceDistance;
          }
        }
      }
      if (bumpedTo != null) {
        bumpedTo.bumpedTime = 300;
        log.println("Bumped to " + bumpedTo.id);
        out.println("Bumped to " + bumpedTo.id);
      }
    }
  }

  private boolean isTooNear(CarPosition opponent, int nextNextSwitchPieceIndex) {
    if (opponent == null) {
      return false;
    }

    double ourTimeTakenToFinish = (track.distanceTo(ourCarPosition, nextNextSwitchPieceIndex) - dimensions.length) / (ourCarPosition.velocity + ourCarPosition.acceleration / 20);
    log.printf("our %f / %f = %f\n", track.distanceTo(ourCarPosition, nextNextSwitchPieceIndex), (ourCarPosition.velocity + ourCarPosition.acceleration / 20), ourTimeTakenToFinish );
    double timeTakenToFinish = track.distanceTo(opponent, nextNextSwitchPieceIndex) / (opponent.velocity  + opponent.acceleration / 20);
    log.printf("opp %f / %f = %f %s\n", track.distanceTo(opponent, nextNextSwitchPieceIndex), (opponent.velocity + opponent.acceleration / 20), timeTakenToFinish, opponent.id.name);
    if (opponent.bumpedTime <= 0 && abs(opponent.velocity - ourCarPosition.velocity) < 0.05 && abs(ourTimeTakenToFinish - timeTakenToFinish) < 3) {
      return false;
    }
    return ourTimeTakenToFinish < 150 && ourTimeTakenToFinish < timeTakenToFinish;
  }

  protected void resetStateBetweenRaces() {
    lastCarPositions = null;
    carPositions = null;
    ourCarPosition = null;
    ourLastCarPosition = null;
    route = null;
    lastRouteSentPiece = -2;
    inTime = null;
  }

  protected abstract void init();

  protected abstract void tick(String gameTick);

  public void handleInit(final MsgWrapper msgFromServer) {
    Race race = msgFromServer.getData("race", Race.class);
    dimensions = race.cars[0].dimensions;
    track = race.track;
    track.raceSession = race.raceSession;
    initSegments();
    calculateRoutes();
    resetStateBetweenRaces();
    System.out.println("Race init:" + track);
    log.println("Race track:" + track);
  }

  private void initSegments() {
    Segment prev = null;
    for (int i = 0; i < track.pieces.size(); ++i) {
      JsonObject obj = track.pieces.get(i).getAsJsonObject();
      Segment segment = new Segment();
      segment.lengths = new double[track.lanes.length];
      JsonElement e = obj.get("length");
      if (e != null) {
        fill(segment.lengths, e.getAsFloat());
      } else {
        segment.isTurn = true;
        segment.radiuses = new double[track.lanes.length];
        segment.angle = obj.get("angle").getAsFloat();
        segment.radius = obj.get("radius").getAsFloat();
        for (Lane lane : track.lanes) {
          double r = segment.angle > 0 ? segment.radius - lane.distanceFromCenter : segment.radius
              + lane.distanceFromCenter;
          segment.radiuses[lane.index] = r;
          segment.lengths[lane.index] = r * 2 * PI * abs(segment.angle) / 360;
        }
      }
      e = obj.get("switch");
      if (e != null) {
        segment.hasSwitch = e.getAsBoolean();
      }
      segment.switchLengths = new double[track.lanes.length][];
      if (segment.isTurn) {
        segment.switchRadiuses = new double[track.lanes.length][];
      }
      for (Lane lane1 : track.lanes) {
        segment.switchLengths[lane1.index] = new double[track.lanes.length];
        if (segment.isTurn) {
          segment.switchRadiuses[lane1.index] = new double[track.lanes.length];
        }
        for (Lane lane2 : track.lanes) {
          double len;
          double radius = 0;
          if (lane1.index == lane2.index) {
            len = segment.lengths[lane1.index];
            if (segment.isTurn) {
              radius = segment.radiuses[lane1.index];
            }
          } else if (segment.hasSwitch && abs(lane1.index - lane2.index) == 1) {
            if (segment.isTurn) {
              len = 0f;
              int steps = 16;
              double r1 = - lane1.distanceFromCenter + segment.radius;
              double r2 = - lane2.distanceFromCenter + segment.radius;
              double rstep = (r1 - r2) / steps;
              for (int j = 0; j < steps; j++) {
                double rstart = r2 + rstep * j;
                double rend = r2 + rstep * (j + 1);
                len = len
                    + sqrt(rstart * rstart + rend * rend - 2 * rstart * rend
                        * cos(segment.angle * PI / 180 / steps));
              }
              radius = len * 180f / Math.PI / segment.angle;
            } else {
              double dist = abs(lane1.distanceFromCenter - lane2.distanceFromCenter) * 1.0105f;
              double length = (segment.lengths[lane1.index] + segment.lengths[lane2.index]) / 2 * 1.0004f;
              len = sqrt(length * length + dist * dist);
            }
          } else {
            len = -1;
          }
          segment.switchLengths[lane1.index][lane2.index] = len;
          if (segment.isTurn) {
            segment.switchRadiuses[lane1.index][lane2.index] = radius;
          }
        }
      }
      segment.id = i;
      segment.prev = prev;
      if (prev != null) {
        prev.next = segment;
      }
      prev = segment;
      track.segments.put(i, segment);
    }
    prev.next = track.segments.get(0);
    track.segments.get(0).prev = prev;
    track.pieces = null;
  }

  private void calculateRoutes() {
    Set<Integer> switches = new LinkedHashSet<>();
    for (Segment s : track.segments.values()) {
      if (s.hasSwitch) {
        switches.add(s.id);
      }
    }
    Set<TrackRoute> trackRoutes = new HashSet<>();
    for (Lane startLane : track.lanes) {
      TrackRoute choice = new TrackRoute();
      choice.endLane = choice.startLane = startLane.index;
      trackRoutes.add(choice);
      recurseTrackLength(trackRoutes, choice);
    }
    routes = new TreeSet<>(trackRoutes);
    double delta = routes.last().length - routes.first().length;
    out.printf("%d different routes, minLength=%f, maxLength=%f (diff=%f aka %.1f%% longer)\n", routes.size(),
        routes.first().length, routes.last().length, delta, delta * 100 / routes.first().length);
    log.printf("%d different routes, minLength=%f, maxLength=%f (diff=%f aka %.1f%% longer)\n", routes.size(),
        routes.first().length, routes.last().length, delta, delta * 100 / routes.first().length);

    shortestRoute = routes.first();

    routes = calculateRouteBoosts(routes);

    // choose best route that starts and ends on same track
    for (TrackRoute route : routes) {
      if (route.switches.isEmpty() || route.startLane == route.endLane) {
        track.bestRoute = route;
        break;
      }
    }

    for (TrackRoute route : routes) {
      calculateTrackSections(route);
    }

    delta = track.bestRoute.length - shortestRoute.length;
    out.printf("best route: length=%f, boost=%f (diff=%f aka %.1f%% longer than shortest)\n", track.bestRoute.length,
        track.bestRoute.boost, delta, delta * 100 / shortestRoute.length);
    log.printf("best route: length=%f, boost=%f (diff=%f aka %.1f%% longer than shortest)\n", track.bestRoute.length,
        track.bestRoute.boost, delta, delta * 100 / shortestRoute.length);

    track.firstRoundRouteForLane = new TrackRoute[track.lanes.length];
    int optimalStartLane = track.bestRoute.startLane;
    for (Lane lane : track.lanes) {
      if (lane.index == optimalStartLane) {
        track.firstRoundRouteForLane[optimalStartLane] = track.bestRoute;
        continue;
      }
      for (TrackRoute route : routes) {
        if (route.startLane == lane.index && route.endLane == optimalStartLane) {
          track.firstRoundRouteForLane[lane.index] = route;
          route.sections.get(route.sections.size() - 1).next = track.bestRoute.sections.get(0);
          break;
        }
      }
    }

    for (TrackRoute route : routes) {
      calculateLongestStraits(route, track.firstRoundRouteForLane[route.endLane].length);
    }

    System.out.println(Joiner.on("\n").join(track.bestRoute.sections));
    System.out.println(Joiner.on("\n").join(track.firstRoundRouteForLane[0].sections));
  }

  private void calculateLongestStraits(TrackRoute r, double endBonusLength) {
    TreeSet<Section> straits = new TreeSet<>(new Comparator<Section>() {
      @Override
      public int compare(Section o1, Section o2) {
        double delta = o2.length - o1.length;
        if (delta > 0) {
          return 1;
        }
        if (delta < 0) {
          return -1;
        }
        return 0;
      }
    });
    for (Section s : r.sections) {
      s.nthLongest = 10000;
      if (s.type == strait && !s.hasSwitch) {
        straits.add(s);
      }
    }
    int nthLongest = 1;
    for (Section s : straits) {
      s.nthLongest = nthLongest++;
    }
  }

  private TreeSet<TrackRoute> calculateRouteBoosts(SortedSet<TrackRoute> routes) {
    TreeSet<TrackRoute> ret = new TreeSet<>();
    for (TrackRoute route : routes) {
      TrackRoute r = route.clone();
      for (SwitchOn change : route.switches) {
        if (track.isTurn(change.pieceId) && !track.isTurn(track.nextSegment(change.pieceId))
            && isSwitchFromInnerToOuterTrack(change)) {
          r.boost -= 5;
        } else {
        }
      }
      ret.add(r);
    }
    return ret;
  }

  private Boolean isSwitchFromInnerToOuterTrack(SwitchOn change) {
    Segment segment = track.segments.get(change.pieceId);
    if (!segment.isTurn) {
      return null;
    }
    return segment.radiuses[change.toTrack] > segment.radiuses[change.fromTrack];
  }

  private void recurseTrackLength(Set<TrackRoute> trackChoices, TrackRoute choice) {
    Segment segment = track.segments.get(choice.piecePosition);
    if (segment == null) {
      return;
    }
    choice.piecePosition++;
    if (segment.hasSwitch) {
      if (choice.endLane > 0) {
        TrackRoute leftChoice = choice.clone();
        int endLane = choice.endLane - 1;
        leftChoice.length += segment.switchLengths[choice.endLane][endLane];
        SwitchOn switchOn = new SwitchOn(segment.id, choice.endLane, endLane);
        leftChoice.switches.add(switchOn);
        leftChoice.endLane = endLane;
        trackChoices.add(leftChoice);
        recurseTrackLength(trackChoices, leftChoice);
      }
      if (choice.endLane < track.lanes.length - 1) {
        TrackRoute rightChoice = choice.clone();
        int endLane = choice.endLane + 1;
        rightChoice.length += segment.switchLengths[choice.endLane][endLane];
        SwitchOn switchOn = new SwitchOn(segment.id, choice.endLane, endLane);
        rightChoice.switches.add(switchOn);
        rightChoice.endLane = endLane;
        trackChoices.add(rightChoice);
        recurseTrackLength(trackChoices, rightChoice);
      }
    }
    choice.length += segment.lengths[choice.endLane];
    recurseTrackLength(trackChoices, choice);
  }

  private void calculateTrackSections(TrackRoute r) {
    r.sections = new ArrayList<>();
    Segment piece = track.segments.get(0);
    Section seg = new Section(piece, 0);
    int lane = r.startLane;
    seg.length = piece.lengths[lane];
    double trackPos = seg.length;
    while ((piece = piece.next).id != 0) {
      Section seg2 = new Section(piece, trackPos);
      int endLane = lane;
      int switchDir = r.getSwitchCommand(piece.id);
      if (switchDir != 0) {
        seg2.hasSwitch = true;
        endLane = lane + switchDir;
      }
      seg2.length = piece.switchLengths[lane][endLane];
      trackPos += seg2.length;
      if (piece.isTurn) {
        seg2.avgRadius = piece.switchRadiuses[lane][endLane];
        seg2.startRadius = piece.radiuses[lane];
        seg2.endRadius = piece.radiuses[endLane];
      }
      if (!seg.add(seg2)) {
        r.sections.add(seg);
        seg.next = seg2;
        seg = seg2;
      }
      lane = endLane;
    }
    seg.next = r.sections.get(0);
    r.sections.add(seg);
  }

  int lastRouteCalcPos = -2;

  protected void parsePositions(MsgWrapper msgFromServer) {
    lastCarPositions = carPositions;
    carPositions = msgFromServer.getData(CarPosition[].class);
    for (int i=0; i<carPositions.length; ++i) {
      CarPosition pos = carPositions[i];
      CarPosition lastPos = lastCarPositions == null ? null : lastCarPositions[i];
      if (ourCarColor.equals(pos.id.color)) {
        ourLastCarPosition = ourCarPosition;
        ourCarPosition = pos;
      } else if (lastPos != null) {
        pos.bumpedTime = lastPos.bumpedTime - 1;
      }
      calculateBasicConstants(pos, lastPos);
    }

    for (CarPosition pos : carPositions) {
      if (pos == ourCarPosition) continue;
      pos.isLeadingUs = pos.piecePosition.isLeading(ourCarPosition.piecePosition);
    }

    if (route == null || lastRouteCalcPos != ourCarPosition.piecePosition.pieceIndex) {
      lastRouteCalcPos = ourCarPosition.piecePosition.pieceIndex;
      lastRoute = route;
      route = getOptimalRoute(ourCarPosition);
      if (route != lastRoute) {
        log.println("Switching route from " + lastRoute + " to " + route + (route == track.bestRoute ? " (best)" : ""));
        out.println("Switching route from " + lastRoute + " to " + route + (route == track.bestRoute ? " (best)" : ""));
      }
    }

    setSectionForPosition(route, ourCarPosition);

    if (ourLastCarPosition != null && ourLastCarPosition.section.firstPiece == ourCarPosition.section.firstPiece && ourLastCarPosition.angle > 0) {
      ourCarPosition.ticksForSlipAngle = ourLastCarPosition.ticksForSlipAngle + 1;
    } else {
      ourCarPosition.ticksForSlipAngle = 1;
    }
  }

  TrackRoute getOptimalRoute(CarPosition car) {
    return getOptimalRoute(car.piecePosition);
  }

  TrackRoute getOptimalRoute(Position position) {
    int optimalStartLane = track.bestRoute.startLane;
    TrackRoute shortestRoute = null;
    double shortestDistance = 100000000.0;
    TrackRoute shortestWithCorrectEndLane = null;
    double shortestWithCorrectEndLaneDistance = 100000000.0;
    for (TrackRoute route : routes) {
      if (route.containsPosition(position, false)) {
        double distance = route.getRemainingLenthFrom(position);
        if (distance < shortestDistance) {
          shortestDistance = distance;
          shortestRoute = route;
        }
        if (route.endLane == optimalStartLane) {
          if (distance < shortestWithCorrectEndLaneDistance) {
            shortestWithCorrectEndLaneDistance = distance;
            shortestWithCorrectEndLane = route;
          }
        }
      }
    }
    return shortestWithCorrectEndLane != null ? shortestWithCorrectEndLane : shortestRoute;
  }

  protected void calculateBasicConstants(CarPosition pos, CarPosition lastPos) {
    Position currentPiece = pos.piecePosition;
    if (lastPos != null) {
      pos.alive = lastPos.alive;
      Position lastPiece = lastPos.piecePosition;
      double travelled = currentPiece.inPieceDistance;
      if (currentPiece.pieceIndex == lastPiece.pieceIndex) {
        travelled -= lastPiece.inPieceDistance;
      } else {
        Segment segment = track.segments.get(lastPiece.pieceIndex);
        travelled += segment.switchLengths[lastPiece.lane.startLaneIndex][lastPiece.lane.endLaneIndex]
            - lastPiece.inPieceDistance;
      }
      pos.velocity = travelled;
      pos.acceleration = pos.velocity - lastPos.velocity;
      pos.angleVelocity = pos.angle - lastPos.angle;
      pos.turbo = lastPos.turbo.clone();
      if (pos.turbo.turboOn) {
        pos.turbo.turboTicksLeft--;
      }
    } else {
      pos.alive = true;
      pos.velocity = 0.0f;
      pos.acceleration = 0.0f;
      pos.angleVelocity = 0.0f;
      pos.ticksForSlipAngle = 1;
    }
  }

  protected void setSectionForPosition(TrackRoute r, CarPosition ourPosition) {
    for (Section s : r.sections) {
      Position currentPiece = ourPosition.piecePosition;
      if (s.firstPiece <= currentPiece.pieceIndex && currentPiece.pieceIndex <= s.lastPiece) {
        ourPosition.section = s;
        ourPosition.sectionPosition = s.startTrackPosition[currentPiece.pieceIndex - s.firstPiece]
            + currentPiece.inPieceDistance;
        break;
      }
    }
  }

  protected TrackRoute getRouteForPiece(TrackRoute currentRoute, Position currentPiece) {
    TrackRoute r = currentRoute;
    if (r == null) {
      r = track.firstRoundRouteForLane[currentPiece.lane.startLaneIndex].clone();
    } else if (currentPiece.lap == 1) {
      r = track.bestRoute.clone();
    } else if (track.raceSession.isLastLap(currentPiece.lap)) {
      r = track.bestRoute.clone();
      r.sections.get(r.sections.size() - 1).length += 10000.0;
    }
    Section bestRouteFirstSection = track.bestRoute.sections.get(0);
    Section ourRouteLastSection = r.sections.get(r.sections.size() - 1);
    if (ourRouteLastSection.addLength(bestRouteFirstSection)) {
      ourRouteLastSection.next = bestRouteFirstSection.next;
    }
    return r;
  }

  protected Section getNextSectionOfType(Section.SectionType type) {
    Section s = ourCarPosition.section;
    while (s.type != type) {
      if (s.next.firstPiece < s.lastPiece) {
        s = track.bestRoute.sections.get(0);
      } else {
        s = s.next;
      }
    }
    return s;
  }

  protected void send(final SendMsg msg) {
    writer.println(msg.toJson(inTime, log));
    writer.flush();
  }
}

abstract class SendMsg {
  static final Gson GSON = new Gson();
  static final SimpleDateFormat df = new SimpleDateFormat("YYYYMMdd-HHmmss.SSS");

  public String toJson(Date inTime, PrintWriter log) {
    String out = GSON.toJson(new SendWrapper(this));
    Date writeTime = new Date();
    long delta = 0;
    if (inTime != null) {
      delta = writeTime.getTime() - inTime.getTime();
    }
    log.println(df.format(writeTime) + " < " + delta + " " + out);
    return out;
  }

  protected Object msgData() {
    return this;
  }

  protected abstract String msgType();

  protected abstract String msgTick();
}

class MsgWrapper {
  static final Gson GSON = new Gson();

  public String msgType;
  public JsonElement data;
  public String gameId;
  public String gameTick;

  public <T> T getData(String name, Class<T> type) {
    return GSON.fromJson(data.getAsJsonObject().get(name), type);
  }

  public <T> T getData(Class<T> type) {
    return GSON.fromJson(data, type);
  }
}

class SendWrapper {
  static final Gson GSON = new Gson();

  public String msgType;
  public Object data;
  public String gameTick;

  public SendWrapper(final SendMsg sendMsg) {
    this.msgType = sendMsg.msgType();
    this.data = sendMsg.msgData();
    this.gameTick = sendMsg.msgTick();
  }
}

class Data {
  public Object race;
}

class BotId {
  public final String name;
  public final String key;

  BotId(final String name, final String key) {
    this.name = name;
    this.key = key;
  }
}

class JoinRace extends SendMsg {
  public final BotId botId;
  public final String trackName;
  public final String password;
  public final int carCount;

  JoinRace(final String botName, final String botKey, final String trackName, final String password, final int carCount) {
    this.botId = new BotId(botName, botKey);
    this.trackName = trackName;
    this.password = password;
    this.carCount = carCount;
  }

  @Override
  protected String msgType() {
    return "joinRace";
  }

  @Override
  protected String msgTick() {
    return null;
  }
}

class CreateRace extends SendMsg {
  public final BotId botId;
  public final String trackName;
  public final String password;
  public final int carCount;

  CreateRace(final String botName, final String botKey, final String trackName, final String password,
      final int carCount) {
    this.botId = new BotId(botName, botKey);
    this.trackName = trackName;
    this.password = password;
    this.carCount = carCount;
  }

  @Override
  protected String msgType() {
    return "createRace";
  }

  @Override
  protected String msgTick() {
    // TODO Auto-generated method stub
    return null;
  }
}

class Join extends SendMsg {
  public final String name;
  public final String key;

  Join(final String name, final String key) {
    this.name = name;
    this.key = key;
  }

  @Override
  protected String msgType() {
    return "join";
  }

  @Override
  protected String msgTick() {
    return null;
  }
}

class Ping extends SendMsg {
  @Override
  protected String msgType() {
    return "ping";
  }

  @Override
  protected String msgTick() {
    return null;
  }
}

class SwitchLane extends SendMsg {
  private String value;

  public SwitchLane(String dir) {
    this.value = dir;
  }

  public SwitchLane(int delta) {
    this(delta < 0 ? "Left" : "Right");
  }

  @Override
  protected String msgType() {
    return "switchLane";
  }

  @Override
  protected Object msgData() {
    return value;
  }

  @Override
  protected String msgTick() {
    return null;
  }
}

class ActivateTurbo extends SendMsg {
  private String tick;

  public ActivateTurbo() {
  }

  public ActivateTurbo(String GameTick) {
    this.tick = GameTick;
  }

  @Override
  protected String msgType() {
    return "turbo";
  }

  @Override
  protected Object msgData() {
    return "Tellus!";
  }

  @Override
  protected String msgTick() {
    return tick;
  }
}

class Throttle extends SendMsg {
  private double value;
  private String tick;

  public Throttle(double value) {
    this.value = value;
  }

  public Throttle(double value, String tick) {
    this.value = value;
    this.tick = tick;
  }

  @Override
  protected Object msgData() {
    return value;
  }

  @Override
  protected String msgType() {
    return "throttle";
  }

  @Override
  protected String msgTick() {
    return tick;
  }
}