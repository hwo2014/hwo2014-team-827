package noobbot;

import static java.lang.Math.abs;
import static java.lang.System.out;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;

import msgs.CarPosition.Position;
import msgs.Section.SectionType;

public class Tiihis extends Main {
  public Map<Integer, Integer> optimalLane = new HashMap<>();
  public double curveSpeed = 5.5f;
  public double curveThrottle = 0.55f;

  public Tiihis(BufferedReader reader, PrintWriter writer) throws IOException {
    super(reader, writer);
  }

  @Override
  protected void tick( String gameTick ) {
    Position pos = ourCarPosition.piecePosition;
    log.printf("%3d / %5.2f @ %6.1f lane=%d velocity=%4.2f nextTurnIn=%5.1f nextStraitIn=%5.1f radius=%5.1f\n", pos.pieceIndex, pos.inPieceDistance, ourCarPosition.angle, pos.lane.startLaneIndex, ourCarPosition.velocity,
        track.distanceToNextTurn(ourCarPosition), track.distanceToNextStrait(ourCarPosition), track.radius(ourCarPosition));
    log.printf("%s / %5.2f @ %6.1f lane=%d velocity=%4.2f nextTurnIn=%5.1f nextStraitIn=%5.1f radius=%5.1f\n", ourCarPosition.section, ourCarPosition.sectionPosition, ourCarPosition.angle, pos.lane.startLaneIndex, ourCarPosition.velocity,
        track.distanceToNextTurn(ourCarPosition), track.distanceToNextStrait(ourCarPosition), track.radius(ourCarPosition));
    out.println(ourCarPosition.section + " " + ourCarPosition.sectionPosition + " - " + getNextSectionOfType(SectionType.turn));
    if (sendRouteSwitchIfNeeded()) {
      return;
    }

    send(new Throttle(0.4f));
/*
    double toNextTurn = track.distanceToNextTurn(ourCarPosition);
    if (toNextTurn == 0) {
      if (track.distanceToNextStrait(ourCarPosition) < 20 && abs(ourCarPosition.angle) < 20) {
        // fast exit from turn if we are not too badly in an angle
        send(new Throttle(1.0));
        return;
      }
      if (abs(ourCarPosition.angle) > 5) {
        // slow down more if our angle is too high
        send(new Throttle(curveThrottle - 0.1f));
      } else if (abs(ourCarPosition.angle) > 15) {
        // slow down more if our angle is too high
        send(new Throttle(0.15));
      } else {
        send(new Throttle(curveThrottle));
      }
      return;
    }
    if (ourCarPosition.velocity > curveSpeed) {
      double extraSpeed =  ourCarPosition.velocity - curveSpeed;
      double breakDistance = extraSpeed * 150;
      double distanceLeft = toNextTurn - breakDistance;
      out.printf("%5.2f %5.2f %5.2f %5.2f\n", ourCarPosition.velocity, extraSpeed, breakDistance, distanceLeft);
      log.printf("%5.2f %5.2f %5.2f %5.2f\n", ourCarPosition.velocity, extraSpeed, breakDistance, distanceLeft);
      if (distanceLeft > 0) {
        send(new Throttle(1.0));
      } else {
        send(new Throttle(curveThrottle));
      }
      return;
    }
    send(new Throttle(1.0));
    */
  }

  @Override
  public void init() {
  }
}
