package noobbot;

import java.io.BufferedReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

import msgs.CarPosition;
import msgs.Section.SectionType;
import msgs.TrackRoute.SwitchOn;
import msgs.Turbo;

public class Rockets extends Main {
  static double maxMagic = 0.05;
  static double MAGIC = 0.0465;
  static final double minMagic = 0.045;
  public static final String LASTRUN_LOG = "Rockets_lastrun.log";
  private static double accToSpeed = 0.02040816f;
  static double maxt = 1.0f;
  // germany
  // static final double MAGIC = 0.0493;
  // finland
  // static final double MAGIC = 0.0463;
  // usa
  // static final double MAGIC = 0.0471;
  static double pt = 1.0f;
  static PrintWriter lastLog;
  CarPosition predicted;

  public Rockets(BufferedReader reader, PrintWriter writer) throws IOException {
    super(reader, writer);
  }

  @Override
  protected void init() {
    try {
      lastLog = new PrintWriter(new FileWriter(LASTRUN_LOG), false);
    } catch (IOException e) {
      System.exit(-1);
    }
  }

  @Override
  protected void handleCrash() {
    lastLog.println("crash");
    System.out.println(ourLastCarPosition);
    System.out.println(ourCarPosition);
    maxMagic = MAGIC;
    MAGIC = MAGIC - (MAGIC - minMagic) / 2.0;
  }

  @Override
  protected void tournamentEnd() {
    super.tournamentEnd();
    System.out.println("MAGIC=" + MAGIC);
  }

  @Override
  protected void tick(String gameTick) {
    if (false && MAGIC + 0.0001 < maxMagic) {
      MAGIC = MAGIC + 0.0001;
    }
    if (sendRouteSwitchIfNeeded()) {
      return;
    }
    if (sendTurboIfNeeded(pt)) {
      return;
    }
    int tick = 0;
    try {
      tick = Integer.parseInt(gameTick);
    } catch (Exception e) {
    }
    if (tick == 4) {
      checkMagicNumber();
    }
    double throttle = findMaxThrottle(ourCarPosition);
    // double throttle = 0.6f;
    throttle = throttle + ourCarPosition.angle / 150f;
    if (throttle > 1f) {
      throttle = 1f;
    }
    if (throttle < 0f) {
      throttle = 0f;
    }
    send(new Throttle(throttle));
    if (predicted != null
        && (Math.abs(ourCarPosition.velocity - predicted.velocity) > 0.001f || Math.abs(ourCarPosition.acceleration
            - predicted.acceleration) > 0.001f)) {
      lastLog.println("---------------------------------------------------------------------------------");
      logCarPosition("r" + tick, ourCarPosition, throttle);
      logCarPosition("p" + tick, predicted, throttle);
    }
    predicted = getNextCarPosition(ourCarPosition, pt);
    logCarPosition("r" + tick, ourCarPosition, throttle);
    logCarPosition("p" + (tick + 1), predicted, pt);
    pt = throttle;
  }

  private void overTakeOrBlockIfNeeded() {
    for (CarPosition enemy : carPositions) {
      if (enemy.id.equals(ourCarPosition.id)) {
        continue;
      } else {
        // jos sama lane tai tulossa samalla
        if (enemy.piecePosition.lane.endLaneIndex == ourCarPosition.piecePosition.lane.endLaneIndex) {
          if (enemy.piecePosition.pieceIndex == ourCarPosition.piecePosition.pieceIndex)
            if (enemy.piecePosition.inPieceDistance + 0.2 > ourCarPosition.piecePosition.inPieceDistance) {
              // jos etäisyys lyhyt ja vihu hitaampi ja edesä tehdään switch
              if (Math.random() < 0.8) { // not always cos we so räikkönen
                send(new SwitchLane(ourCarPosition.piecePosition.lane.startLaneIndex == 0
                    || ourCarPosition.piecePosition.lane.startLaneIndex < track.lanes.length - 1 ? "Right" : "Left"));
              }
            }
        }
      }
    }
  }

  private boolean sendTurboIfNeeded(double throttle) {
    CarPosition nextCarPosition = getNextCarPosition(ourCarPosition, pt);
    nextCarPosition = getNextCarPosition(ourCarPosition, pt);
    nextCarPosition = getNextCarPosition(ourCarPosition, pt);
    nextCarPosition = getNextCarPosition(ourCarPosition, pt);
    if (ourCarPosition.turbo.turboFactor > 0f && !track.isTurn(nextCarPosition.piecePosition.pieceIndex)
        && isSafeToActivateTurbo(nextCarPosition) && getNextSectionOfType(SectionType.strait).nthLongest < 3
        && !ourCarPosition.turbo.turboOn) {
      send(new ActivateTurbo());
      ourCarPosition.turbo.turboOn = true;
      return true;
    }
    return false;
  }

  private boolean isSafeToActivateTurbo(CarPosition carPosition) {
    CarPosition nextCarPosition = getNextCarPosition(carPosition, maxt);
    nextCarPosition.turbo.turboOn = true;
    return isSafe(nextCarPosition, 0.25f);
  }

  private void checkMagicNumber() {
    System.out.println("MN=" + ourCarPosition.acceleration / (10f * pt - ourCarPosition.velocity));
    accToSpeed = ourCarPosition.acceleration / (10f * pt - ourCarPosition.velocity);
  }

  private double findMaxThrottle(CarPosition carPosition) {
    CarPosition realNextCarPosition = getNextCarPosition(carPosition, pt);
    double ttmax = maxt;
    double ttmin = 0f;
    double tt = ttmax;
    int maxTry = 5;
    while (maxTry > 0) {
      CarPosition nextCarPosition = getNextCarPosition(realNextCarPosition, tt);
      maxTry--;
      if (isSafe(nextCarPosition, 0.25f)) {
        if (tt == ttmax) {
          return ttmax;
        }
        ttmin = tt;
      } else {
        ttmax = tt;
      }
      tt = (ttmin + ttmax) / 2f;
    }
    return ttmin;
  }

  private boolean isSafe(CarPosition nextCarPosition, double throttle) {
    if (track.segments.get(nextCarPosition.piecePosition.pieceIndex) == null) {
      return true;
    }
    int ticksToCheck = 40;
    CarPosition positionToBeChecked = nextCarPosition;
    for (int i = 0; i < ticksToCheck; i++) {
      int pieceIndex = positionToBeChecked.piecePosition.pieceIndex;
      if (track.isTurn(pieceIndex) && positionToBeChecked.velocity > maxSpeedAtTurn(positionToBeChecked)) {
        return false;
      }
      positionToBeChecked = getNextCarPosition(positionToBeChecked, throttle);
    }
    return true;
  }

  private double maxSpeedAtTurn(CarPosition carPosition) {
    return Math.sqrt(track.radius(carPosition) * MAGIC * (dimensions.getAngularD()));
  }

  private double getAcceleration(double velocity, double throttle, double turboFactor) {
    return -velocity * accToSpeed + accToSpeed * 10f * throttle * turboFactor;
  }

  private double getNextSpeed(double velocity, double throttle, double turboFactor) {
    return (velocity + accToSpeed * 10f * throttle * turboFactor) / (1f + accToSpeed);
  }

  private CarPosition getNextCarPosition(CarPosition currentPosition, double throttle) {
    CarPosition nextPosition = new CarPosition();
    double turboFactor = 1f;
    nextPosition.turbo = currentPosition.turbo.clone();
    if (currentPosition.turbo.turboOn) {
      nextPosition.turbo.turboTicksLeft--;
      if (nextPosition.turbo.turboTicksLeft <= 0) {
        nextPosition.turbo = new Turbo();
      }
    }
    if (nextPosition.turbo.turboOn) {
      turboFactor = nextPosition.turbo.turboFactor;
    }
    nextPosition.velocity = getNextSpeed(currentPosition.velocity, throttle, turboFactor);
    nextPosition.acceleration = getAcceleration(nextPosition.velocity, throttle, turboFactor);
    nextPosition.angle = getNextAngle(currentPosition, throttle);
    nextPosition.angleVelocity = getNextAngleVelocity(currentPosition, throttle);
    nextPosition.piecePosition = new CarPosition.Position();
    nextPosition.piecePosition.pieceIndex = currentPosition.piecePosition.pieceIndex;
    nextPosition.piecePosition.lap = currentPosition.piecePosition.lap;
    nextPosition.piecePosition.lane = new CarPosition.Lane();
    double nipd = nextPosition.velocity + currentPosition.piecePosition.inPieceDistance;
    double nipdd = nipd
        - track.segments.get(currentPosition.piecePosition.pieceIndex).lengths[currentPosition.piecePosition.lane.startLaneIndex];
    if (nipdd > 0) {
      nextPosition.piecePosition.pieceIndex++;
      nextPosition.piecePosition.lane.startLaneIndex = currentPosition.piecePosition.lane.endLaneIndex;
      nextPosition.piecePosition.inPieceDistance = nipdd;
      if (nextPosition.piecePosition.pieceIndex >= track.segments.size()) {
        nextPosition.piecePosition.pieceIndex = 0;
        nextPosition.piecePosition.lap++;
      }
    } else {
      nextPosition.piecePosition.lane.startLaneIndex = currentPosition.piecePosition.lane.startLaneIndex;
      nextPosition.piecePosition.inPieceDistance = nipd;
    }
    nextPosition.piecePosition.lane.endLaneIndex = nextPosition.piecePosition.lane.startLaneIndex;
    for (SwitchOn switchOn : route.switches) {
      if (switchOn.pieceId == nextPosition.piecePosition.pieceIndex) {
        nextPosition.piecePosition.lane.endLaneIndex = switchOn.toTrack;
        break;
      }
      if (switchOn.pieceId > nextPosition.piecePosition.pieceIndex) {
        break;
      }
    }
    return nextPosition;
  }

  private double getNextAngleVelocity(CarPosition currentPosition, double throttle) {
    // TODO Auto-generated method stub
    return 0;
  }

  private double getNextAngle(CarPosition currentPosition, double throttle) {
    // TODO Auto-generated method stub
    return 0;
  }

  private void logCarPosition(String gameTick, CarPosition carPosition, double throttle) {
    lastLog.print(gameTick);
    lastLog.print(",");
    lastLog.print(carPosition.piecePosition.lap);
    lastLog.print(",");
    lastLog.print(carPosition.piecePosition.lane.startLaneIndex);
    lastLog.print(",");
    lastLog.print(carPosition.piecePosition.lane.endLaneIndex);
    lastLog.print(",");
    lastLog.print(carPosition.piecePosition.pieceIndex);
    lastLog.print(",");
    lastLog.print(carPosition.piecePosition.inPieceDistance);
    lastLog.print(",");
    lastLog.print(carPosition.velocity);
    lastLog.print(",");
    lastLog.print(carPosition.acceleration);
    lastLog.print(",");
    lastLog.print(carPosition.angle);
    lastLog.print(",");
    lastLog.print(carPosition.angleVelocity);
    lastLog.print(",");
    lastLog.print(track.avgRadius(carPosition.piecePosition.pieceIndex, carPosition.piecePosition.lane.startLaneIndex,
        carPosition.piecePosition.lane.endLaneIndex));
    lastLog.print(",");
    lastLog.print(track.segments.get(carPosition.piecePosition.pieceIndex).isTurn ? track.segments
        .get(carPosition.piecePosition.pieceIndex).angle : 0);
    lastLog.print(",");
    lastLog.print(throttle);
    lastLog.print(",");
    lastLog.print(carPosition.turbo.turboOn);
    lastLog.print(",");
    lastLog.print(carPosition.turbo.turboTicksLeft);
    lastLog.print(",");
    lastLog.print(carPosition.turbo.turboFactor);
    lastLog.println();
  }

  public class Crash extends RuntimeException {
  }
}
