package noobbot;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;

import msgs.CarPosition;
import msgs.Section.SectionType;

public class MrAverage extends AbstractBot {
  private static final double ANGULAR_MULTI = 2.0f;
  private static final double TURD_DIVIDER = 0.8f;
  double velosum;
  long velocount;
  double max_angle;
  int max_angle_tick;
  double abs_angle_sum;
  long abs_angle_count;
  public static final String LASTRUN_LOG = "Rockets_lastrun.log";

  static double maxt = 1.0f;
  static double pt = 1.0f;
  double prevSlipAngle;
  CarPosition predicted;

  public MrAverage(BufferedReader reader, PrintWriter writer) throws IOException {
    super(reader, writer);
  }

  @Override
  protected void handleCrash() {
    C_UG*=0.98;
    System.out.println( "Le crash");
    System.out.println(ourLastCarPosition);
    System.out.println(ourCarPosition);
  }

  @Override
  protected void tournamentEnd() {
    super.tournamentEnd();
  }

  @Override
  protected void tick(String gameTick) {

    int tick = 0;
    try {
      tick = Integer.parseInt(gameTick);
    } catch (Exception e) {
    }
    sendRouteSwitchIfNeeded();
    sendTurboIfNeeded(pt,gameTick);
    overTakeOrBlockIfNeeded(gameTick);
    checkMagicNumbers(tick);
    checkAngularMagicNumbers();

    if( Math.abs(ourCarPosition.angle)>max_angle ) {
      max_angle=Math.abs(ourCarPosition.angle);
      max_angle_tick=tick;
      }

    abs_angle_sum+=Math.abs(ourCarPosition.angle);
    abs_angle_count++;

    double throttle = 1f;
    CarPosition realNextCarPosition = getNextCarPosition(ourCarPosition, pt);
    if(track.isTurn(ourCarPosition.piecePosition.pieceIndex)
        &&  track.distanceToNextStrait(ourCarPosition)<track.radius(ourCarPosition)*0.5
        && checkPendulumSafe(ourCarPosition)) {
      throttle=1.0f;
    }
    else if ( needBreak(realNextCarPosition) ){
      throttle = 0.0f;
    } else {
      if ( track.isTurn(ourCarPosition.piecePosition.pieceIndex)) {
        throttle=( ourCarPosition.velocity<(maxSpeedAtTurn(ourCarPosition)+0.2) && checkPendulumSafe(ourCarPosition))?1.0:maxSpeedAtTurn(ourCarPosition)/maxSpeed;
      }
    }

    velosum+=ourCarPosition.velocity;
    velocount++;
    String msg = String.format("[%4d|%2d|%1d] T[%2.3f] V[%2.2f(%2.2f)|%2.2f][%3.2f] A[%2.2f|%3.2f]R[%2.1f] L[%1d|%1d] S[%2.2f|%2.2f] a[%2.2f|%3d][%2.2f] XX[%2.4f|%2.4f|%2.4f]\n",
        tick, ourCarPosition.piecePosition.pieceIndex, ourCarPosition.piecePosition.lap,
        throttle,
        ourCarPosition.velocity,
        maxSpeedAtTurn(ourCarPosition),
        velosum/velocount,
        ourCarPosition.angleVelocity,
        ourCarPosition.angle,track.segments.get(ourCarPosition.piecePosition.pieceIndex).angle,track.segments.get(ourCarPosition.piecePosition.pieceIndex).radius,
        ourCarPosition.piecePosition.lane.startLaneIndex,ourCarPosition.piecePosition.lane.endLaneIndex,
        track.distanceToNextTurn(ourCarPosition),track.distanceToNextStrait(ourCarPosition),
        max_angle,max_angle_tick,abs_angle_sum/abs_angle_count,
        ANGULAR_MULTI,TURD_DIVIDER,C_UG);
    System.out.print(msg);
    log.print(msg);
    if (throttle > 1f)
      throttle = 1f;
    if (throttle < 0f)
      throttle = 0f;
    send(new Throttle(throttle));
    if (predicted != null
        && (ourCarPosition.piecePosition.lap != predicted.piecePosition.lap
            || Math.abs(ourCarPosition.piecePosition.inPieceDistance - predicted.piecePosition.inPieceDistance) > 0.001f
            || Math.abs(ourCarPosition.velocity - predicted.velocity) > 0.001f || Math.abs(ourCarPosition.acceleration
            - predicted.acceleration) > 0.001f)) {
    }
    predicted = getNextCarPosition(ourCarPosition, pt);
    pt = throttle;
  }


  @Override
  protected void overTakeOrBlockIfNeeded( String gametick ) {
    for (CarPosition enemy : carPositions) {
      if (enemy.id.equals(ourCarPosition.id)) {
        continue;
      } else {
        // jos sama lane tai tulossa samalla
        if (enemy.piecePosition.lane.endLaneIndex == ourCarPosition.piecePosition.lane.endLaneIndex) {
          if (enemy.piecePosition.pieceIndex == ourCarPosition.piecePosition.pieceIndex)
            if (enemy.piecePosition.inPieceDistance + 0.2 > ourCarPosition.piecePosition.inPieceDistance) {

              //if corner near and we fast enough, we try to turbo bump to oblivion
              if( 50.0<track.distanceToNextTurn(ourCarPosition) && track.distanceToNextTurn(ourCarPosition)<200.0 && enemy.velocity<(ourCarPosition.velocity*1.2) ) {
                send(new ActivateTurbo(gametick));
              }
              else if (Math.random() < 0.8) { // not always cos we so räikkönen
                send(new SwitchLane(ourCarPosition.piecePosition.lane.startLaneIndex == 0
                    || ourCarPosition.piecePosition.lane.startLaneIndex < track.lanes.length - 1 ? "Right" : "Left"));
              }
            }
        }
      }
    }
  }

  private boolean sendTurboIfNeeded(double throttle, String gametick) {
    CarPosition nextCarPosition = getNextCarPosition(ourCarPosition, pt);
    if (ourCarPosition.turbo.turboFactor > 0f && isSafeToActivateTurbo(nextCarPosition)
        && getNextSectionOfType(SectionType.strait).nthLongest < 2
        && getNextSectionOfType(SectionType.strait).nthLongest > 0 && !ourCarPosition.turbo.turboOn
        && ( track.radius(ourCarPosition)== 0.0 || (track.distanceToNextStrait(ourCarPosition)<(track.radius(ourCarPosition)*0.5)))) {
      send(new ActivateTurbo(gametick));
      ourCarPosition.turbo.turboOn = true;
      return true;
    }
    return false;
  }

  @Override
  protected boolean isSafeToActivateTurbo(CarPosition carPosition) {
    carPosition.turbo.turboOn = true;
    return isSafe(carPosition);
  }

  private boolean isSafe(CarPosition nextCarPosition) {
    if (track.segments.get(nextCarPosition.piecePosition.pieceIndex) == null) {
      return true;
    }
    int ticksToCheck = 40;
    CarPosition positionToBeChecked = nextCarPosition;
    for (int i = 0; i < ticksToCheck; i++) {
      int pieceIndex = positionToBeChecked.piecePosition.pieceIndex;
      if (track.isTurn(pieceIndex) && positionToBeChecked.velocity > maxSpeedAtTurn(positionToBeChecked)) {
        return false;
      }
      positionToBeChecked = getNextCarPosition(positionToBeChecked, 0f);
    }
    return true;
  }

  @Override
  protected void init() {
    // TODO Auto-generated method stub

  }

  @Override
  protected double maxSpeedAtTurn(CarPosition carPosition) {
    return Math.sqrt(track.radius(carPosition) * C_UG);
  }


}
