package noobbot;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import msgs.CarPosition;
import msgs.TurboHistory;
import msgs.Section.SectionType;
import msgs.Track.Segment;

public class HMSEnterprison extends AbstractBot {
  private static final double MIN_THROTTLE = 0.0;
  public static final String LASTRUN_LOG = "Rockets_lastrun.log";
  CarPosition predicted;
  private double RISK_FACTOR = 1.5;
  private double CHEN_RISK_FACTOR = 1.43;
  private static int crash_count = 0;
  private static double vEPSILON = 0.05;
  private static double sEPSILON = 8.0;
  private static double aEPSILON = 25.0;
  private static int UPDATE_COUNTER = 5000; // ticks to update
  double CHEN_C_UG = 0.35;
  double BIG_MOD = 0.0007;
  private final int MAX_ANGLE = 50;

  List<TurboHistory> turbohistory = new ArrayList<TurboHistory>();
  int lastTurboChecked=0;

  double velosum;
  long velocount;
  double max_angle;
  int max_angle_tick;
  double abs_angle_sum;
  long abs_angle_count;
  String type;
  int tick=0;

  public HMSEnterprison(BufferedReader reader, PrintWriter writer) throws IOException {
    super(reader, writer);
  }

  @Override
  protected void init() {

  }

  @Override
  protected void handleCrash() {
    String state=!turbohistory.isEmpty()?turbohistory.get(turbohistory.size()-1).turboSegment+"":"";
    System.out.println(turbohistory.isEmpty() + " " + state );
    if( ourCarPosition.turbo.turboOn || (!turbohistory.isEmpty()&& lastTurboChecked!=turbohistory.size()-1 && turbohistory.get(turbohistory.size()-1).turboTick+((turbohistory.get(turbohistory.size()-1).turboDuration+20)) <tick )) {
      System.out.printf( "\n\tCRASHED DUE TO TURBO, MARKING SEGMENT %3d invalid for throttle %3.3f and turbofactor x\n\n",
          turbohistory.get(turbohistory.size()-1).turboSegment, turbohistory.get(turbohistory.size()-1).turboPreThrottle);
      turbohistory.get(turbohistory.size()-1).turboWasSafe=false;
      lastTurboChecked=turbohistory.size()-1;
    }
    else { //We only adapt if no turbo was used
    abs_angle_sum=0.0;
    abs_angle_count=0L;
    max_angle=0;
    switch( crash_count ) {
    //start with 1.5 = hugerisk
    case 0:
      C_UG*=0.93;
      RISK_FACTOR*=0.75;
    break;
    case 1:
      C_UG*=0.95;
      RISK_FACTOR*=0.90;
    break;
    case 2:
      C_UG*=0.98;
      RISK_FACTOR*=0.95;
    break;
    case 3:
      C_UG*=0.99;
      RISK_FACTOR*=0.95;
    break;
    default:
      C_UG*=0.995;
      RISK_FACTOR*=0.95;
      UPDATE_COUNTER = 0;
    }
    }
    System.out.printf("Crashed before %3d myyG=%3.3f risk=%3.3f",crash_count,C_UG, RISK_FACTOR);
    crash_count++;
  }

  private double updateMagic(double risk, double maxBeta, int maxBetaPos, double averBeta) {
    double newMagic = risk;
    if (maxBeta>58.0 && (abs_angle_sum/abs_angle_count)>aEPSILON*2.0) {
      newMagic = risk * (1.0-BIG_MOD*10.0);
    }
    else if( maxBeta > 55.0 && (abs_angle_sum/abs_angle_count)>aEPSILON) {
      newMagic = risk * (1.0-BIG_MOD);
    } else {
      newMagic = risk * (1.0/(1.0-BIG_MOD));
    }
    return newMagic;
  }

  @Override
  protected void tournamentEnd() {
    super.tournamentEnd();
  }


  private double getMaxAngle() {
    return MAX_ANGLE - (CHEN_C_UG - 0.35) * 70 - (maxSpeed - 10);
  }

  private boolean hasBiggerTurnComing(CarPosition realNextCarPosition) {
    CarPosition nextCarPosition = getNextCarPosition(realNextCarPosition, 1);
    int ticksToCheck = 3 + (int) ((maxSpeed - 7) / 2);
    for (int i = 0; i < ticksToCheck; i++) {
      nextCarPosition = getNextCarPosition(nextCarPosition, 1);
    }
    if (nextCarPosition.piecePosition.pieceIndex == realNextCarPosition.piecePosition.pieceIndex) {
      return false;
    }
    if (track.isTurn(nextCarPosition.piecePosition.pieceIndex)) {
      return track.radius(realNextCarPosition) > track.radius(nextCarPosition);
    }
    return false;
  }

protected double rightBrainTick(String gameTick) {

    CarPosition realNextCarPosition = ourCarPosition;
    double throttle = findMaxThrottle(realNextCarPosition);
    if (track.isTurn(realNextCarPosition.piecePosition.pieceIndex) && !hasBiggerTurnComing(realNextCarPosition)) {
      double maxSpeedAtTurnWithoutSlip = maxSpeedAtTurnWithoutSlip(realNextCarPosition);
      double maxSpeedAtTurn = maxSpeedAtTurnChineseWay(realNextCarPosition);
      if (ourCarPosition.angle * ourCarPosition.angleVelocity < 0) {
        throttle = throttle + ourCarPosition.angle / 35;
      } else {
        double absAngle = Math.abs(ourCarPosition.angle);
        if (absAngle > getMaxAngle()) {
          throttle = 0;
        } else {
          throttle = getThrottleForSpeedChange(maxSpeedAtTurnWithoutSlip + (getMaxAngle() - absAngle)
              * (maxSpeedAtTurn - maxSpeedAtTurnWithoutSlip) / getMaxAngle(), realNextCarPosition.velocity, 1);
        }
      }
    }

    if (tick > 4
        && predicted != null
        && (ourCarPosition.piecePosition.lap != predicted.piecePosition.lap
            || Math.abs(ourCarPosition.piecePosition.inPieceDistance - predicted.piecePosition.inPieceDistance) > 0.001
            || Math.abs(ourCarPosition.velocity - predicted.velocity) > 0.001 || Math.abs(ourCarPosition.acceleration
            - predicted.acceleration) > 0.001)) {
    }
    if (throttle > 1)
      throttle = 1;
    if (throttle < 0)
      throttle = 0;
    if (tick < 4)
      throttle = 1;

  return throttle;
}

  protected double leftBrainTick(String gameTick) {

    // timing and averages
    long start = System.currentTimeMillis();
    velosum += ourCarPosition.velocity;
    velocount++;

    if (ourCarPosition.alive && track.isTurn(ourCarPosition.piecePosition.pieceIndex)) {
      if (Math.abs(ourCarPosition.angle) > max_angle) {
        max_angle = Math.abs(ourCarPosition.angle);
        max_angle_tick = tick;
      }

      // we update in first laps
      if (ourCarPosition.velocity > 0.0 && --UPDATE_COUNTER > 0
          && track.isTurn(ourCarPosition.piecePosition.pieceIndex) && !ourCarPosition.turbo.turboOn) {
        RISK_FACTOR = updateMagic(RISK_FACTOR, max_angle, max_angle_tick, abs_angle_sum / abs_angle_count);
      }

      abs_angle_sum += Math.abs(ourCarPosition.angle);
      abs_angle_count++;
      BIG_MOD*=0.998;
    }

    double max_speed_next_turn = maxSpeedAtNextTurn(track.nextTurn(ourCarPosition));
    double dV = (ourCarPosition.velocity + vEPSILON) - max_speed_next_turn;
    double a = getAcceleration(ourCarPosition.velocity, 0.0, ourCarPosition.turbo.turboFactor);
    double breaking_time = dV < 0 ? 0.0 : dV / a;
    double breaking_distance = (ourCarPosition.velocity * breaking_time + 0.5 * Math.pow(breaking_time, 2) * a);
    breaking_distance = breaking_distance > 0.0 ? breaking_distance + sEPSILON : breaking_distance;

    double throttle = 1.0;
    CarPosition realNextCarPosition = getNextCarPosition(ourCarPosition, prevThrottle);

    if (track.isTurn(ourCarPosition.piecePosition.pieceIndex)
        && track.distanceToNextStrait(ourCarPosition) < track.radius(ourCarPosition) * 0.55) {
      type = "OUT OF BEND:";

      double distance_to_next_turn_after_straight=track.distanceTo(ourCarPosition,track.nextTurn(track.nextStrait(ourCarPosition)));
      double speed_next = maxSpeedAtNextTurn(track.nextTurn(track.nextStrait(ourCarPosition)));
      dV = (ourCarPosition.velocity + vEPSILON) - speed_next;
      a = getAcceleration(ourCarPosition.velocity, 0.0, ourCarPosition.turbo.turboFactor);
      breaking_time = dV < 0 ? 0.0 : dV / a;
      breaking_distance = (ourCarPosition.velocity * breaking_time + 0.5 * Math.pow(breaking_time, 2) * a);
      breaking_distance = breaking_distance > 0.0 ? breaking_distance + sEPSILON : breaking_distance;
      breaking_distance -= distance_to_next_turn_after_straight;
      throttle = ((breaking_distance) > sEPSILON) ?
          safeThrottle(MIN_THROTTLE) : safeThrottle(1.0);

    } else  if (!track.isTurn(ourCarPosition.piecePosition.pieceIndex))
      {
      type = "CASE JOLLY STRAIGHT:";
      boolean chenSuggestsToBreak = needBreak(realNextCarPosition);
      boolean larsSuggestToBreak = track.distanceToNextTurn(ourCarPosition) + breaking_distance < sEPSILON;
      type += chenSuggestsToBreak + " " + larsSuggestToBreak;
      throttle = (chenSuggestsToBreak && larsSuggestToBreak) ? safeThrottle(MIN_THROTTLE)  : (chenSuggestsToBreak || larsSuggestToBreak) ? safeThrottle(0.99):safeThrottle(1.0);
    } else {
      CarPosition nextCarPosition = getNextCarPosition(ourCarPosition, 0f);

      type = "STRAIT:";
      throttle=safeThrottle(1.0);
      if (track.isTurn(nextCarPosition.piecePosition.pieceIndex)) {

        int nextAfter = (ourCarPosition.section.lastPiece + 1) % track.segments.size();
        Segment s = track.segments.get(nextAfter);
        boolean next_section_narrower_turn = s.isTurn
            && s.radiuses[ourCarPosition.piecePosition.lane.endLaneIndex] < track.radius(ourCarPosition);
        double distanceToNarrower = (next_section_narrower_turn) ? track.distanceTo(ourCarPosition, nextAfter)
            : 10000000.0;
        double speed_next = maxSpeedAtNextTurn(nextAfter);
        dV = (ourCarPosition.velocity + vEPSILON) - speed_next;
        a = getAcceleration(ourCarPosition.velocity, 0.0, ourCarPosition.turbo.turboFactor);
        breaking_time = dV < 0 ? 0.0 : dV / a;
        breaking_distance = (ourCarPosition.velocity * breaking_time + 0.5 * Math.pow(breaking_time, 2) * a);
        breaking_distance = breaking_distance > 0.0 ? breaking_distance + sEPSILON : breaking_distance;
        type = "TURN:" + distanceToNarrower + breaking_distance;

        throttle = ((distanceToNarrower + breaking_distance) < sEPSILON) ? safeThrottle(MIN_THROTTLE)
            : maxSpeedAtTurnNoRisk(ourCarPosition) / maxSpeed;
      }
    }

    if (tick < 4) {
      throttle = 1f;
      }

    throttle=throttle>1.0?1.0:throttle<0.0?0.0:throttle;

    if (tick > 4
        && predicted != null
        && (ourCarPosition.piecePosition.lap != predicted.piecePosition.lap
            || Math.abs(ourCarPosition.piecePosition.inPieceDistance - predicted.piecePosition.inPieceDistance) > 0.001f
            || Math.abs(ourCarPosition.velocity - predicted.velocity) > 0.001f || Math.abs(ourCarPosition.acceleration
            - predicted.acceleration) > 0.001f)) {
    }


    // logging
    long delta = System.currentTimeMillis() - start;
    velosum += ourCarPosition.velocity;
    velocount++;
    String msg = String
        .format(
            "[%4d|%2d|%1d] T[%2.3f] %s(%2d) %3.5f V[%2.2f(%2.2f|%2.2f)|%2.2f][%3.2f] A[%2.2f|%3.2f]R[%2.1f] L[%1d|%1d] S[%2.2f|%2.2f] a[%2.2f|%3d][%2.2f/%2.2f] XX[%2.4f|%2.4f] %4dms %s %3d\n",
            tick, ourCarPosition.piecePosition.pieceIndex, ourCarPosition.piecePosition.lap, throttle,
            ourCarPosition.alive ? "alive" : "crashed", crash_count,
            (1.0-BIG_MOD),
            ourCarPosition.velocity,
            maxSpeedAtTurn(ourCarPosition), maxSpeedAtNextTurn(track.nextTurn(ourCarPosition)), velosum / velocount,
            ourCarPosition.angleVelocity, ourCarPosition.angle,
            track.segments.get(ourCarPosition.piecePosition.pieceIndex).angle,
            track.segments.get(ourCarPosition.piecePosition.pieceIndex).radius,
            ourCarPosition.piecePosition.lane.startLaneIndex, ourCarPosition.piecePosition.lane.endLaneIndex,
            track.distanceToNextTurn(ourCarPosition), track.distanceToNextStrait(ourCarPosition), max_angle,
            max_angle_tick, abs_angle_sum / abs_angle_count, aEPSILON, C_UG, RISK_FACTOR, delta, type, UPDATE_COUNTER);
    System.out.print(msg);
    log.print(msg);

    return throttle;
  }

  private double safeThrottle(double proposedThrottle) {
    return track.isTurn(ourCarPosition.piecePosition.pieceIndex) ? (ourCarPosition.velocity < (maxSpeedAtTurn(ourCarPosition) + ourCarPosition.acceleration)) ? proposedThrottle
        : checkPendulumSafe(ourCarPosition) ? proposedThrottle
            : ourCarPosition.velocity > maxSpeedAtTurnNoRisk(ourCarPosition) ? 0.0 : maxSpeedAtTurnNoRisk(ourCarPosition)
                / maxSpeed
        : proposedThrottle;
  }


  private boolean sendTurboIfNeeded(double throttle, int gametick) {
    CarPosition nextCarPosition = getNextCarPosition(ourCarPosition, prevThrottle);
    if (ourCarPosition.turbo.turboFactor > 0f
        && checkTurboHistory()
        && isSafeToActivateTurbo(nextCarPosition)
        && checkPendulumSafe(ourCarPosition)
        && getNextSectionOfType(SectionType.strait).nthLongest < 2
        && getNextSectionOfType(SectionType.strait).nthLongest > 0
        && !ourCarPosition.turbo.turboOn
        && (track.radius(ourCarPosition) == 0.0 || (track.distanceToNextStrait(ourCarPosition) < (track
            .radius(ourCarPosition) * 0.5)))) {
      send(new ActivateTurbo(gametick+""));
      ourCarPosition.turbo.turboOn = true;
      TurboHistory fresh = new TurboHistory();
      fresh.turboPreThrottle=throttle;
      fresh.turboSegment=ourCarPosition.piecePosition.pieceIndex;
      fresh.turboWasSafe=true;
      fresh.turboTick=gametick;
      turbohistory.add(fresh);
      return true;
    }
    return false;
  }

  private double findMaxThrottle(CarPosition carPosition) {
    double ttmax = maxt;
    double ttmin = 0f;
    double tt = ttmax;
    int maxTry = 5;
    while (maxTry > 0) {
      CarPosition nextCarPosition = getNextCarPosition(carPosition, tt);
      maxTry--;
      if (isSafe(nextCarPosition)) {
        if (tt == ttmax) {
          return ttmax;
        }
        ttmin = tt;
      } else {
        ttmax = tt;
      }
      tt = (ttmin + ttmax) / 2;
    }
    return ttmin;
  }

  private boolean isSafe(CarPosition nextCarPosition) {
    if (track.segments.get(nextCarPosition.piecePosition.pieceIndex) == null) {
      return true;
    }
    int ticksToCheck = 38 + (int) ((maxSpeed - 7) * 1.5);
    CarPosition positionToBeChecked = nextCarPosition;
    for (int i = 0; i < ticksToCheck; i++) {
      int pieceIndex = positionToBeChecked.piecePosition.pieceIndex;
      if (track.isTurn(pieceIndex) && positionToBeChecked.velocity > maxSpeedAtTurnChineseWay(positionToBeChecked)) {
        return false;
      }
      positionToBeChecked = getNextCarPosition(positionToBeChecked, 0);
    }
    return true;
  }


  private boolean checkTurboHistory() {
    int count = 0;
   for( TurboHistory turbo: turbohistory ) {
     if( turbo.turboSegment == ourCarPosition.piecePosition.pieceIndex && !turbo.turboWasSafe ) {
       count+=2;
     }
     else if( !turbo.turboWasSafe) {
       count++;
     }
   }
   //if failred in this segment or failed twice dont use turbo
   return count>=2?false:true;
  }

  protected double maxSpeedAtNextTurn(int index) {
    return Math.sqrt(track.radius(index, ourCarPosition.piecePosition.lane.endLaneIndex,
        ourCarPosition.piecePosition.lane.endLaneIndex, 0) * C_UG * RISK_FACTOR);
  }


  protected double maxSpeedAtTurnChineseWay(CarPosition carPosition) {
    double mspeed = Math.sqrt(track.radius(carPosition) * CHEN_C_UG * CHEN_RISK_FACTOR);
    return mspeed;
  }


  protected double maxSpeedAtNextTurnNoRisk(int index) {
    return Math.sqrt(track.radius(index, ourCarPosition.piecePosition.lane.endLaneIndex,
        ourCarPosition.piecePosition.lane.endLaneIndex, 0) * C_UG );
  }

  protected double maxSpeedAtTurnWithoutSlip(CarPosition carPosition) {
    double mspeed = Math.sqrt(track.radius(carPosition) * CHEN_C_UG / 1.5 * CHEN_RISK_FACTOR);
    return mspeed;
  }

  @Override
  protected double maxSpeedAtTurn(CarPosition carPosition) {
    return Math.sqrt(track.radius(carPosition) * C_UG * RISK_FACTOR);
  }

  protected double maxSpeedAtTurnNoRisk(CarPosition carPosition) {
    return Math.sqrt(track.radius(carPosition) * C_UG );
  }

  @Override
  protected void overTakeOrBlockIfNeeded(String gametick) {
    for (CarPosition enemy : carPositions) {
      if (enemy.id.equals(ourCarPosition.id)) {
        continue;
      } else {
        // jos sama lane tai tulossa samalla
        if (enemy.piecePosition.lane.endLaneIndex == ourCarPosition.piecePosition.lane.endLaneIndex) {
          if (enemy.piecePosition.pieceIndex == ourCarPosition.piecePosition.pieceIndex)
            if (enemy.piecePosition.inPieceDistance > ourCarPosition.piecePosition.inPieceDistance) {
              if (Math.random() < 0.99) { // not always cos we so räikkönen
                send(new SwitchLane(ourCarPosition.piecePosition.lane.startLaneIndex == 0
                    || ourCarPosition.piecePosition.lane.startLaneIndex < track.lanes.length - 1 ? "Right" : "Left"));
              }
            }
        }
      }
    }
  }

  @Override
  protected void tick(String gameTick) {

    try {
      tick = Integer.parseInt(gameTick);
    } catch (Exception e) {
    }

    double lars = leftBrainTick(gameTick);
    double chen = rightBrainTick(gameTick);
    double variable_weight = crash_count*0.20;
    variable_weight=(variable_weight>1.0)?1.0:variable_weight;

    sendRouteSwitchIfNeeded();
    sendTurboIfNeeded(prevThrottle, tick);
    overTakeOrBlockIfNeeded(gameTick);
    checkMagicNumbers(tick);
    setSlipAngleConstant();
    checkAngularMagicNumbers();

    double throttle =variable_weight*chen+(1.0-variable_weight)*lars;
    if( crash_count>5 ) {
      double crashweight = 1.0-(0.05*crash_count);
      crashweight=crashweight<=0.5?0.5:crashweight;
      throttle=safeThrottle(crashweight*throttle);
    }
    System.out.printf( "Weighted=%3.3f L:%3.3f C:%3.3f W=%3.3f", throttle,lars, chen, variable_weight);
    send(new Throttle(throttle, gameTick));

    predicted = getNextCarPosition(ourCarPosition, prevThrottle);
    prevThrottle = throttle;
    prevSlipAngle = ourCarPosition.angle;
  }

}
