package analyzer;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

import msgs.CarPosition;

import com.google.gson.Gson;

public class HistoryAnalyzer {

  //20140417-102051.148 < 1 {"msgType":"throttle","data":1.0,"gameTick":"1232"}
  //20140417-102051.171 > {"msgType":"carPositions","data":[{"id":{"name":"Salamannopeus","color":"red"},
  //"angle":20.110702819747626,"piecePosition":{"pieceIndex":23,"inPieceDistance":26.163729372481786,"lane":{"startLaneIndex":0,"endLaneIndex":0},"lap":2}}],"gameId":"8bdbdaaa-d2a7-4c2e-806c-c7f9d626172c","gameTick":1233}
  //20140417-102051.171 < 0 {"msgType":"throttle","data":1.0,"gameTick":"1233"}
  public static void main( String[] args ) throws IOException  {
    
    final Gson gson = new Gson();
    //loop files
    String str = null;
    BufferedReader in = null;
    try {
      in = new BufferedReader( new FileReader( "Salamannopeus_20140417-102013.242.log" ));
    } catch (FileNotFoundException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }
    while( (str=in.readLine())!=null ) {

      if( str.indexOf("throttle")!= -1 ) {
        //System.out.println( str );
        str = str.substring(24);
        Throttle t = gson.fromJson(str, Throttle.class);
        System.out.println( "\t"+t.gameTick + "\t"+ t.data );
      }
      else if( str.indexOf("carPositions")!=-1 ) {
        str = str.substring(22);
       //System.out.println( str );
        CarPos c = gson.fromJson(str, CarPos.class);
        System.out.print( c.data[0].piecePosition.pieceIndex + "\t" + c.data[0].piecePosition.inPieceDistance +"\t"
        +c.gameTick + "\t" + c.data[0].piecePosition.lap +"\t" + c.data[0].piecePosition.lane.startLaneIndex +"\t"+ c.data[0].piecePosition.lane.endLaneIndex);
        }
      else if( str.indexOf("crash")!=-1 ) {
        System.out.println( "CRASHED");
      }
      else if( str.indexOf("lapFinished")!=-1 ) {
        System.out.println( str );
      }
    }
  }
//  "data":[
  //{"id":{ "name":"Salamannopeus","color":"red"},
  //    "angle":-0.7403588666030756,
  //    "piecePosition":{
  //        "pieceIndex":39,
  //        "inPieceDistance":54.88986849278389,
  //         "lane":{"startLaneIndex":0,"endLaneIndex":0},"lap":2}}],"gameId":"8bdbdaaa-d2a7-4c2e-806c-c7f9d626172c","gameTick":1431}
  class Lane {
    int startLaneIndex;
    int endLaneIndex;
  }
  class PiecePosition {
    int pieceIndex;
    double inPieceDistance;
    Lane lane;
    int lap;
  }
  
  class Data {
    Id id;
    double angle;
    PiecePosition piecePosition;
  }
  
  class Id {
    String name;
    String color;
  }
  
  class CarPos {
    String msgType;
    String gameId;
    String gameTick;
    Data[] data;
  }
  
  
    
    class Throttle{
       String msgType;
       double data;
       String gameTick;
  
    }
  
  
}
